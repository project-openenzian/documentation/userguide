\chapter{Assembly}

\section{Chassis Preparation}
\label{s:chassis_prep}

The standard chassis for an Enzian system is a lightly customised Chenbro
RM238 2U rackmount case, with a 24-slot 2.5" hotswap array attached to 2
8-port NVMe backplanes and 1 8-port SATA.  The steps required to adapt the
chassis from its as-shipped condition and mount an Enzian mainboard are
illustrated in this section, and include: Replacing one of the 3 shipped NVMe
backplanes with a SATA one, installing the PSU and dividing and rebundling its
cables, extending chassis fan cables 3 and 4, and routing and dressing all
chassis cabling.

\paragraph*{Equipment Required}
\begin{enumerate}
\item Chenbro RM23824H01*14526 chassis
\item FSP1200-50ERS PSU
\item SATA backplane PCB
\item Cable bundling tape e.g. Tesa 51618
\item Open-barrel connector crimp tool e.g. Knipex 97 43 200
\item Parts for 8-pin power extension: $0.75\text{mm}^2$ (19AWG) stranded wire
(black + yellow), Molex 39-01-2080 + 8x39-00-0039 (female), Molex 39-01-3083 +
8x39-00-0041 (male)
\item Parts for extended chassis fan cables: 24AWG stranded wire (black, blue,
yellow, red), 2xMolex 47054-1000, 8xMolex 08-50-0114
\item Quad SATA to SFF-8643 cable, 1M e.g. Delock 83322
\item Cable ties and adhesive mounts
\item Fabric tape
\item Side-cutting pliers
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Unbox the RM238 chassis, retaining all packaging for later reshipment,
and inspect for damage.
\item Remove the mainboard section top cover by removing the two screws marked
in figure \ref{rm238_closed} (one on either side), and unfastening the black
latch (cover slides toward the rear).

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_closed.jpg}
\caption{Opening the RM238\label{rm238_closed}}
\end{figure}

\item Remove the disk array cover by pressing the two silver tabs and sliding
it forward.

\clearpage

\item Remove the fan bar by loosening the two thumbscrews marked in figure
\ref{rm238_backplane_supplier}, sliding it towards the rear, and lifting.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_backplane_supplier.jpg}
\caption{Removing the fan bar\label{rm238_backplane_supplier}}
\end{figure}

\item \label{step:remove_array} Remove the disk array from the chassis by
removing the two screws marked in figure (one on each side)
\ref{rm238_array_screws}, sliding it towards the front, and lifting.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_array_screws.jpg}
\caption{Disk array retaining screws\label{rm238_array_screws}}
\end{figure}

The array is held in place by the 4 retaining tabs indicated in figure
\ref{rm238_tabs_in}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_array_tabs_in.jpg}
\caption{Disk array retaining tabs\label{rm238_tabs_in}}
\end{figure}

As shown in figure \ref{rm238_tabs_close}, the tabs are disengaged when the
array itself moves forward, allowing it to be lifted out vertically.

\begin{figure}
\includegraphics[width=0.5\textwidth]{figures/RM238_array_tabs_in_close.jpg}
\includegraphics[width=0.5\textwidth]{figures/RM238_array_tabs_out.jpg}
\caption{Disk array retaining tabs, engaged and
disengaged.\label{rm238_tabs_close}}
\end{figure}

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_array_removed.jpg}
\caption{Disk array removed from chassis\label{rm238_array_removed}}
\end{figure}

\item The backplane PCBs are now easily accessible (figure
\ref{rm238_array_removed}).

\item Remove the two retaining screws from the leftmost NVMe PCB as indicated
in figure \ref{rm238_bp_nvme_screws}.  Remove the PCB by sliding it up until
the metal hooks disengage, then remove toward the rear.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_backplane_NVMe_factory.jpg}
\caption{NVMe backplane PCB retaining screws\label{rm238_bp_nvme_screws}}
\end{figure}

\item Install the replacement SATA module in the same location by reversing
the preceeding steps, securing it with the two retaining screws indicated in
figure \ref{rm238_bp_sata_screws}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_array_backplane_sata_screws_in.jpg}
\caption{SATA backplane PCB retaining screws\label{rm238_bp_sata_screws}}
\end{figure}

\item Reinstall the disk array by reversing step \ref{step:remove_array}.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_USB_tie_mounts.jpg}
\caption{Front-panel USB cable-tie mounts\label{rm238_usb_tie_mounts}}
\end{figure}

\item Affix adhesive cable-tie mounts to the rear shelf of the reinstalled
disk array as shown in figure \ref{rm238_usb_tie_mounts}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_USB_folded.jpg}
\caption{Front-panel USB cable dressing\label{rm238_usb_folded}}
\end{figure}

\item Carefully fold the blue front-panel USB cable as indicated in figure
\ref{rm238_usb_folded}, taking care not to kink the cable.  Secure with cable
ties as shown.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_mb_tray_standoffs.jpg}
\caption{Motherboard standoff locations\label{rm238_mb_standoffs}}
\end{figure}

\item Using the perforations in the motherboard insulation film, clear the
screw holes shown in figure \ref{rm238_mb_standoffs} and install the
motherboard standoffs, taking care not to tear the film.  Tighten the
standoffs with a flat-bladed screwdriver, but do not over tighten (a little
more than finger torque is enough).  These are ATX/SSI EEB locations
A,C,F,G,H,J,X,Y,Z.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_bundles.jpg}
\caption{PSU cable bundles\label{rm238_psu_bundles}}
\end{figure}

\item Arrange the PSU cable harnesses into 3 bundles, as shown in figure
\ref{rm238_psu_bundles}, with as little interweaving between bundles as
possible.

The red bundle (left) will form the drive power harness (P5, P7, P8).  The
green bundle (center) is the FPGA EPS12V (P3), together with unused
connectors.  The blue bundle (right) is the ATX24 connector (P1), the CPU
EPS12V (P2), and the PSU PMBus connector (P9).

\item Working carefully with side-cut pliers, and making sure not to damage
the wire insulation, remove the existing cable ties from all harnesses in the
red bundle (drive power).

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_BP_power_wrap_start.jpg}
\caption{Drive power harness wrapping\label{rm238_bp_power_wrap_start}}
\end{figure}

\item Removing any kinks from the wires and pulling the bundle taught, begin
wrapping the drive power harness as close as practical to the PSU (see figure
\ref{rm238_bp_power_wrap_start}).

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_BP_power_wrap_branch.jpg}
\caption{Wrapping a harness branch\label{rm238_bp_power_wrap_branch}}
\end{figure}

\item Approximately 10cm before each connector, bend its wires away from the
main bundle and continue wrapping only the remaining wires, as shown in figure
\ref{rm238_bp_power_wrap_branch}.

\item Continue wrapping until 10cm before the final connector.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_BP_power_wrap_finish.jpg}
\caption{Reinforcing harness branches\label{rm238_bp_power_wrap_finish}}
\end{figure}

\item Return and add an extra turn over each branch for reinforcement, as
indicated in figure \ref{rm238_bp_power_wrap_finish}.

\clearpage

\item Remove both hot-swap modules from the PSU cage.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_front_bracket_holes.jpg}
\caption{PSU front bracket mounting\label{rm238_psu_front_bracket_holes}}
\end{figure}

\item Using the two supplied M3 screws, attach the PSU front bracket to the
PSU cage as shown in figure \ref{rm238_psu_front_bracket_holes}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_rear_bracket_installed.jpg}
\caption{PSU rear bracket mounting\label{rm238_psu_rear_bracket_installed}}
\end{figure}

\item Mount the PSU rear bracket with the supplied countersunk screws in the
locations marked in figure \ref{rm238_psu_rear_bracket_installed}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_position.jpg}
\caption{PSU cage alignment\label{rm238_psu_position}}
\end{figure}

\item Align the PSU cage the the rear mounting bracket as shown in figure
\ref{rm238_psu_position}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_screws_left.jpg}
\caption{PSU cage mounting screws, left\label{rm238_psu_screws_left}}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_screws_right.jpg}
\caption{PSU cage mounting screws, right\label{rm238_psu_screws_right}}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_PSU_screws_bottom.jpg}
\caption{PSU cage mounting screws, bottom\label{rm238_psu_screws_bottom}}
\end{figure}

\item Secure the PSU bracket to the chassis using the 6 supplied countersunk
screws.  The mounting holes are at the left rear (figure
\ref{rm238_psu_screws_left}), right rear (figure
\ref{rm238_psu_screws_right}), and on the underside of the chassis (figure
\ref{rm238_psu_screws_bottom}).  The screws on the underside mate with the
PSU front mounting bracket.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_fans_reinstalled.jpg}
\caption{Fan numbering and cable routing\label{rm238_fans_reinstalled}}
\end{figure}

\item Reinstall the fan bar as shown in figure \ref{rm238_fans_reinstalled}.
Remove fan cables 3 and 4, and ensure that cable 1 and 2, and the front panel
IO cable are routed through the upper cable channel, as depicted.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_BP_power_routing.jpg}
\caption{Drive power harness routing\label{rm238_bp_power_routing}}
\end{figure}

\item Feed the drive power harness through the lower routing channel and
attach the connectors to the backplane.  Route the harness snugly against the
blue USB cable as shown in figure \ref{rm238_bp_power_routing}.  It's OK to
bend the power wires quite tightly to get a nice fit.  The harness should be
stiff enough to remain in place without additional support.

\clearpage

\item Assemble replacement cables for fans 3 and 4, of length 630mm and 730mm
respectively.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_fan_bundle_end.jpg}
\caption{Fan cable bundling\label{rm238_fan_bundle_end}}
\end{figure}

\item Using a sharp knife, carefully remove the barcode labels from fan cables
1 and 2.

\item Connect cables 3 and 4, and route via the upper cable channel.

\item Label mainboard-end fan connectors with fan number \emph{before}
bundling.

\item Using cable ties, bundle all fan cables such that their mainboard ends
are aligned, as depicted in figure \ref{rm238_fan_bundle_end}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_fan_bundle_ties.jpg}
\caption{Fan cable bundle detail\label{rm238_fan_bundle_ties}}
\end{figure}

\item At the fan bar end, route and bundle the cables as shown in figure
\ref{rm238_fan_bundle_ties}.  Excess length on cable 1 can be taken up in a
loop as seen at the right-hand end of the figure.

\item Tuck the fan cable bundle snugly behind the backplane power harness.  It
should be sufficiently rigid without additional support.

\clearpage

\item Install the IO bracket.  Ensure that it is full inserted into the
chassis cutout.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_MB_installed.jpg}
\caption{Enzian mainboard installation\label{rm238_mb_installed}}
\end{figure}

\item Install the Enzian mainboard as shown in figure
\ref{rm238_mb_installed}.

Supporting the board between the CPU and FPGA heatsinks, tilt it slightly
toward the rear of the case. Insert the QSFP cages into their corresponding
cutouts in the IO bracket, and ensure that they are fully inserted before
lowering the motherboard onto the standoffs.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_IO_with_MB.jpg}
\caption{QSFP alignment\label{rm238_io_with_mb}}
\end{figure}

The QSFP cages should protrude approximately 1cm, until the spring fingers are
fully outside the chassis, as in figure \ref{rm238_io_with_mb}.  This may
require careful finger pressure on the outside of the IO bracket, which is
comparatively flimsy.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_IO_tabs.jpg}
\caption{IO shield grounding tabs\label{rm238_io_tabs}}
\end{figure}

If the IO shield grounding tabs marked in \ref{rm238_io_tabs} have been bent
in shipment, they may bind on the USB connectors.  If so, bend them upward
slightly with long-nose pliers before installing the motherboard.

\clearpage

\item Affix 3 adhesive cable tie mounts in the locations shown in figure
\ref{rm238_mb_installed}: One behind the PSU cage (under the bundled cables),
one between PSU and mainboard, and the third between fan 2 and the mainboard.

\item Fold and secure the unused cables of the center bundle as shown,
together with the FPGA EPS12V connector (P3), which must be routed as shown
between fans and mainboard.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_ATX24_routing_top.jpg}
\caption{CPU power harness (top)\label{rm238_atx24_routing_top}}
\end{figure}

\item Connect ATX24 (P1), CPU EPS12V (P2), and PSU PMBus (P9) to the
mainboard, and secure as shown in figure \ref{rm238_atx24_routing_top}.

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_ATX24_routing_side.jpg}
\caption{CPU power harness (side)\label{rm238_atx24_routing_side}}
\end{figure}

\item Ensure that the harness does not protrude above the top of the chassis
(see figure \ref{rm238_atx24_routing_side}).

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_8pin_bundled.jpg}
\caption{FPGA EPS12V extension\label{rm238_8pin_bundled}}
\end{figure}

\item Using 8 50cm lengths of AWG19 stranded wire (4 black, 4 yellow),
populate a male EPS12V connector (Molex 39-01-3083).

\item Mate this connector to the female FPGA EPS12V connector (P3), and route
and bundle the harness as shown in \ref{rm238_8pin_bundled}.

\item Trim the harness to length, leaving enough for a short relief loop and
attach the female connector (Molex 39-01-2080) to the mainboard end.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_fan_panel_pwr_routing.jpg}
\caption{FPGA-side cable routing\label{rm238_fan_panel_pwr_routing}}
\end{figure}

\item Route EPS12V extension, front-panel IO and fan cables as shown in figure
\ref{rm238_fan_panel_pwr_routing}, and connect all to the mainboard.  Note
strain-relief loops.  Ensure fans are connected correctly (mainboard headers
aren't in numerical order).  Use a high-adhesive-strength tape to support the
cables in the horizontal depression along the chassis wall.

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_sata_routing_top.jpg}
\caption{SATA cable routing\label{rm238_sata_routing_top}}
\end{figure}

\item Route quad SATA to SFF-8643 cable as shown in
\ref{rm238_sata_routing_top} to the leftmost SFF-8643 connector on the SATA
backplane.  Ensure that the SATA ports are connected in order.

\item Gently form the SATA cable into a relief loop on the right-hand side of
the chassis, ensuring it does not protrude above the level of the chassis
cover, and gently secure it together with the FPGA EPS12V harness to the
adjacent cable-tie mount.  \emph{Do not cinch tight.}

\clearpage

\begin{figure}
\includegraphics[width=\textwidth]{figures/RM238_all_routing_top.jpg}
\caption{Overview of cable routing and dressing\label{rm238_all_routing_top}}
\end{figure}

\end{enumerate}
