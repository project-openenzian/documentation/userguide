\chapter{Enzian Overview}

\section{What is Enzian?}

Enzian is a powerful computer for doing systems software research. 
You can use a single Enzian on its own, or connect a rack of them together.

Enzian is a cache-coherent 2-node asymmetric NUMA system where one node is a 48-core Marvell Thunder-X CPU and one node is a Xilinx Virtex Ultrascale+ FPGA. 
It has a maximum of 640GiB of DDR4 RAM, and has up 480Gb/s of network bandwidth, both split between the two nodes. 

\subsection*{Node 0 (CPU)}
\begin{itemize}[noitemsep,nolistsep]
	\item Marvell Cavium ThunderX-1 CN8890-NT CPU @ 2 GHz (48 x ARMv8.1 cores)
	\item 128 GiB DDR4 -- 4x 32 GiB DIMMS @ 1866 MT/s
	\item PCIe Gen3 x8 slot
	\item 3 x NVMe connectors
	\item 4 x SATA connectors
	\item 2 x 40Gb/s Ethernet QSFP28 connectors
	\item USB3, serial UARTs.
	\item JTAG
\end{itemize}

\subsection*{Node 1 (FPGA)}
\begin{itemize}[noitemsep,nolistsep]
	\item Xilinx Virtex Ultrascale+ FPGA XCVU9P-FLGB2104-E
	\item 512 GiB DDR4 (4x 128 GiB DIMMs @ 2133 MT/s) or 64 GiB DDR4 (4 x 16 GiB DIMMs @ 2400 MT/s)
	\item PCIe Gen3 x16 slot
	\item 1 x NVMe connector
	\item FMC connector
	\item 16 x 25Gb/s serial lines in 4 x QSFP28 cages, configurable as 16 25Gb/s or 4 x 100Gb/s Ethernet.
	\item JTAG
\end{itemize}

\subsection{\gls{eci}}
\begin{itemize}[noitemsep,nolistsep]
	\item 24 x 10Gb/s lanes
	\item MOESI-like directory-based protocol
	\item Inter-processor interrupts
	\item Cached and uncached (I/O) accesses
	\item 128-byte cache line size
	\item Shared physical address space
\end{itemize}

\subsection*{\gls{bmc}}

\begin{itemize}[noitemsep,nolistsep]
\item Enclustra Mercury SoM
\item Xilinx Zynq MPSoc CPU running Linux
\item Dedicated 1Gb/s RJ45 Ethernet
\item JTAG
\item All system serial ports brought out as USB UARTs
\end{itemize}

\subsection*{Software}
\begin{itemize}[noitemsep,nolistsep]
\item Ubuntu Linux 20.04 LTS, other OSes should boot.
\item ARM Trusted Firmware
\item Board development kit for low-level access
\end{itemize}

\subsection*{Form factor}
\begin{itemize}[noitemsep,nolistsep]
\item eATX-format motherboard (305 x 330 mm / 12 x 13 in)
\item 2U Rackmount case
\item Can accomodate double-width PCIe cards with riser
\item 1200W Power supply
\end{itemize}

\section{Why is Enzian?}

We’ve built our own computer for research (and making it available to other academic research groups) because of the way that computer hardware in industry is heading.

\subsection*{Custom or reconfigurable hardware is being deployed at scale}

There’s increasing commercial use of custom or reconfigurable hardware. You can rent FPGAs from cloud providers. Large Internet companies use custom ASICs for machine learning. Processor vendors are incorporating application-specific accelerators onto their CPUs. Search providers are using FPGAs to accelerate a variety of online services. Data storage and processing appliances and “tin-wrapped software” increasingly exploit ASICs and FPGAs internally. Some companies are even deploying custom networking hardware.

This is possible because of improvements in CAD tools, emulators, hardware simulators, manufacturing and prototyping processes, and FPGAs themselves.

It’s worth doing this commercially due to large economies of scale, and the vast amounts of data that are processed by modern data centers, from training machine learning models to large-scale data analytics.

\subsection*{Academic systems software research using COTS hardware is unrealistic}

This leaves academic computer scientists working in systems in a bind. For the last few decades, they have used essentially the same computing platforms as industry applications: commodity PCs, commodity Ethernet switches and NICs, commodity GPUs, etc.

The hardware hasn’t qualitatively changed much, just got a lot faster (and more recently, more parallel). When everything is a PC, or a cluster of PCs, it’s easy to figure out what kind of platform to target with your research.

The problem now is that custom and reconfigurable hardware is so radically different, both in its absolute performance and its implications for how you design system software, that research using COTS equipment is increasingly unrealistic. It’s certainly not forward-looking.

\subsection*{Academic systems software research on custom hardware is difficult but has low value}

It’s a real challenge for many Universities to get hold of this kind of new hardware. In many cases, it’s not intended to leave the company at all (i.e. it’s only used internally or accessed in a limited way as a cloud service). Even if you can get the hardware in a university research group, getting the documentation you need for it can be even more difficult. Then you have to learn how to program a new, custom platform which is likely to be obsolete in a couple of years.

But even if you do all that, you’re left doing research on rails. The hardware was designed to do one thing (or class of things) well, and wasn’t intended for other uses. It’s likely that whoever paid for it to be developed is already exploiting it pretty effectively for this purpose, and so if academic researchers are going to do anything radically innovative using the cool new hardware, they’re likely to be fighting a priori design decisions all the way.

\subsection*{There is no good existing platform for relevant, impactful system software research in the medium to long term}

If academics can’t do relevant, impactful, and medium-to-long-term system software research using commodity platforms, and they can’t do it using someone else’s cost-optimized application-specific custom hardware, what can they do?

Our response is to build Enzian: a computer which is not designed for machine learning, or databases, or computer vision, or bitcoin mining, but rather one optimized for exploring the design space for custom hardware/software co-design.

That means Enzian is over-engineered relative to any off-the-shelf hardware, and it’s optimized for flexibility and configurability rather than unit code, efficiency, or performance along any particular dimension.

This still leaves a lot of room for system design choices. Inevitably, we had to make compromises with Enzian - we’re a university, after all. But this philosophy still drives the design.


\section{Where is Enzian?}

Enzian is a project of the Systems Group in the Computer Science Department (D-INFK) at ETH Z\"{u}rich. 
The website is \url{http://enzian.systems}.

\section{Who is Enzian?}


