\chapter{Test Procedures} \label{cha:test_procedures}

% Netnotes can be used to document NetOS/Systems group specific instructions to make our lives a lot easier.
\newcommand\netnote[2][]{\todo[inline, caption={2do}, #1]{
	\begin{minipage}{\textwidth-4pt}\underline{NetOS-Particularity}\newline #2\end{minipage}}}

\section{Hardware Manufacturing Tests}
\label{s:manuf_test}

The tests described in this section are performed on all newly-manufactured
Enzian systems in order to catch manufacturing errors.  They may also be of
use in debugging known or suspected hardware failures.

The attached checklist (XXX attach it) may be of use.

\subsection{Visual Inspection} \label{s::manuf_test:visual}

The board is to be visually inspected for manufacturing errors (e.g. missing
or incorrectly-assembled components), and for damage in shipping (e.g. signs
of impact or crushing).  Any board failing visual inspection is to be rejected
without being powered on.

\paragraph*{Procedure}
\begin{enumerate}
\item Perform visual inspection of the board, and note findings.
\item Note the Enzian (production) series number, manufacturer and
manufacturer's serial number, along with the BMC module serial number.
\item Assign and record the board's serial number within its production
series.
\end{enumerate}

\subsection{First Power Up} \label{s:manuf_test:first_power_up}

The first power-on tests must be conducted with adjustable current-limited
supplies.  This allows the safe diagnosis of errors such as shorts to ground
on the major power nets without the risk of fire, injury, or further damage.
The instantaneous power available on the 12V rails of a server PSU can easily
exceed 1kW, which if dissipated in a short circuit, failed or incorrectly
installed power transistor etc., can easily cause a fire or shrapnel-producing
explosion.  You have been warned.

These instructions assume that the BMC module has been pre-programmed with
both a bitstream and software image.

\paragraph*{Requirements}
\begin{enumerate}
\item Adjustable current-limited supplies.
\item The latest stable BMC firmware:
    \begin{itemize}
        \item \verb+nor-enzianbmc-zx5.bin+ -- Bootloader image
        \item u-boot-enzianbmc-zx5.elf -- First stage bootloader, required for flashing
        \item \verb+bmc/enzianbmc-zx5.dtb+ --- BMC Linux device tree
        \item \verb+bmc/uImage-enzianbmc-zx5.bin+ --- BMC Linux kernel
        \item \verb+bmc/obmc-phosphor-image-enzianbmc-zx5.ubifs+ --- BMC root filesystem
    \end{itemize}
    The latest stable BMC firmware can be \href{https://gitlab.inf.ethz.ch/project-openenzian/bmc/openbmc/-/jobs/artifacts/stable/download?job=build-zx5}{downloaded as an archive from our CI server}.
    The relevant files are in \verb+build/enzianbmc-<commit hash>+ inside the archive.
\item Linux host with installed Xilinx toolchain (Vivado and Vitis 2020.1 or
later).
\item An Ethernet connection to a network with a DHCP server, and a TFTP
server with the BMC Linux device tree, the BMC Linux kernel and the BMC root filesystem
available in a directory called \verb+bmc/+ at the TFTP root.
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Configure JTAG bypass jumpers (J54, J61, J67, J73) such that the BMC is
connected and all others are bypassed.
\item Set DIP switches SW2 and SW3 for a core clock of 2.0GHz
(\verb+PLL_MUL=0x22+) a coprocessor clock of 800MHz (\verb+SYS_PLL_MUL=0xA+),
boot method SPI24 untrusted (\verb+BOOTSTRAP=0x5+, \verb+TRUSTED_MODE=0+).
Note that the switches pull low when on, follow the silkscreen markings:
\verb+SW2=01000101+, \verb+SW3=01010100+ (ordered 1--8).
\item Supply 5V on the 5VSB rail of the main ATX power connector (J49), with a
current limit of no more than 3A.
\item LEDs 1--8 should flash on briefly.
\item Check that the current drawn is less than 1A.
\item Connect a USB cable from J81 (JTAG) to a Linux host, and check for a new
Digilent device in \verb+dmesg+.  Note the serial number.
\netnote{Alternatively, \begin{enumerate}
		\item Connect the JTAG USB cable to the lab-laptop
		\item Open a Powershell-window
		\item Start hw\_server.bat
		\item Note the IP address of the laptop as LAB\_IP
		\item Use the argument `-url tcp:\{LAB\_IP\}:3121' for all Xilinx tools that want to connect to the JTAG port.
\end{enumerate}}
		\netnote{If you do not have access to a Vivado/Vitis cli tool on your machine, `enzian-build.ethz.ch` has Vivado and Vitis installations in `/opt/Xilinx/\{Vivado,Vitis\}`.
		The CLI binaries are in `\{Vivado,Vitis\}/\{VERSION\}/bin`.}
\item Verify that the Vivado toolchain can access the BMC.  Run the following
command (adjust path as necessary):
\begin{verbatim}
program_flash -jtagtargets
\end{verbatim}
If the board to be flashed is attached to a remote machine running the Vivado
hardware server, rather than the local machine, add the argument
\verb+-url tcp:<hostname>:<port>+ (port is \verb+3121+ by default).

On success, you should see output similar to:
\begin{verbatim}
JTAG chain configuration
--------------------------------------------------
1 Digilent XXXXXXXXXXXXX
        2    jsn-JTAG-SMT3-XXXXXXXXXXXXX-YYYYYYYY-0 \
                    (name arm_dap  idcode YYYYYYYY)
        3    jsn-JTAG-SMT3-XXXXXXXXXXXXX-ZZZZZZZZ-0 \
                    (name xc7z015 idcode ZZZZZZZZ)
\end{verbatim}
The digits \verb+XXXX...+ are the embedded JTAG adapter ID, and the first 12
should match the USB device serial number recorded in the previous step.
Digits \verb+YYYY...+ identify the ARM Debug Access Port.
\item Flash the bootloader with the following command (with \verb+XXXX...+ and
\verb+YYYY...+ replaced to match the \verb+arm_dap+ identifier in the previous
output, and set server address if necessary):
\begin{verbatim}
program_flash -f nor-enzianbmc-zx5.bin -flash_type qspi_single \
              -blank_check -verify -fsbl u-boot-enzianbmc-zx5.elf \
              -target_name jsn-JTAG-SMT3-XXXXXXXXXXXXX-YYYYYYYY-0
\end{verbatim}
Look for the following output on success:
\begin{verbatim}
Verify Operation successful.

Flash Operation Successful
\end{verbatim}

\netnote{
If `program\_flash` fails (for example because the flashing failed once), or if a safer, more general approach is desired, the following replaces `program\_flash`:
\begin{enumerate}
\item Connect a USB cable from J3 (UART) to a Linux host, and watch
`dmesg` for another Digilent device, to which 4 USB serial port devices should be assigned.  The first of these is the BMC serial console.
\item Connect to the BMC serial console, for example with:
	`picocom -f n -b 115200 /dev/ttyUSB\{N\}`
\item Retrieve the following files from the archive you downloaded from the CI server in the Requirements section:
	\begin{itemize}
		\item `ps7\_init-enzianbmc-zx5.tcl`
		\item `bitstream-enzianbmc-zx5.bit`
	\end{itemize}
\item Open the Xilinx debugger `xsdb`
\item Connect to the lab computer with `connect -host \{LAB\_IP\}`
\item Run `targets` and note the ID of ``MPCORE \#0''. This is usually $2$.
\item Run `target \{ID\}`.
\item Run `rst -system -stop` to reset the BMC-CPU and hold it there.
\item Run `source ps7\_init-enzianbmc-zx5.tcl` (with the path to the .tcl script)
\item Run `ps7\_init`
\item Run `fpga bitstream-enzianbmc-zx5.bit` (with the path to the .bit file)
\item Run `ps7\_post\_config`
\item Run `dow u-boot-einzianbmc-zx5.elf` (with the path to the .elf file)
\item Be ready to switch to the BMC console after continuing CPU execution. Read ahead in the instructions now to know what to do.
\item Run `con` to continue BMC-CPU execution.
\item Complete steps \ref{netnote:custom-flash:start} up to and including \ref{netnote:custom-flash:end}.
\item Run `run update\_fsbl`
\item Run `run update\_bootimage`
\item Continue with step \ref{netnote:custom-flash:contd}
\end{enumerate}
If any of these steps fail, restart from `rst -system -stop` and/or get help.
}

\item Connect a USB cable from J3 (UART) to a Linux host, and watch
\verb+dmesg+ for another Digilent device, to which 4 USB serial port devices
should be assigned.  The first of these is the BMC serial console.
\item Connect to the BMC serial console, for example with:
\begin{verbatim}
picocom -f n -b 115200 /dev/ttyUSB<N>
\end{verbatim}
\item Use the following command to reset the BMC over the USB JTAG interface
(replace \verb+XXXX...+ with the JTAG adapter ID):
\begin{verbatim}
xsdb -eval 'connect -host localhost -port 3121; \
    targets -set -filter \
    {jtag_cable_ctx=="jsn-JTAG-SMT3-XXXXXXXXXXXXX" \
    && name=="APU"}; rst'
\end{verbatim}

As for \verb+program_flash+, replace \verb+localhost+ with the hardware
server's address if required.
\item \label{netnote:custom-flash:start}Watch the BMC console for u-boot to start.  When the following appears,
press a key to interrupt boot.
\begin{verbatim}
Hit any key to stop autoboot:
\end{verbatim}
You should then see the u-boot prompt:
\begin{verbatim}
Enzian BMC ZX5>
\end{verbatim}
\item Connect an Ethernet cable to J4 and check the link LEDs on the board and
the switch.
\item Compute the BMC MAC address according to the rules in
\ref{sec:mac_calculation}.  Bits 47--24 are the fixed Enzian OUI
\verb+0C:53:31+, and bits 4--0 are \verb+0b11000+, indicating the BMC primary
interface.  Bits 23--16 encode the production series and 15--5 the board
serial number assigned earlier.

For example, board 2 of series 3 has BMC MAC address \verb+0C:53:31:03:00:58+.
\item Set the MAC address:
\begin{verbatim}
setenv ethaddr 0C:53:31:AA:BB:CC; saveenv
\end{verbatim}
\item\label{netnote:custom-flash:end} At the u-boot prompt type:
\begin{verbatim}
dhcp
\end{verbatim}
Verify that the BMC obtains an IP address e.g.
\begin{verbatim}
DHCP client bound to address 192.168.1.XXX (YYY ms)
\end{verbatim}
You can ignore any errors about files not found as long as the above line appears with a sensible IP.
\item \label{netnote:custom-flash:contd}
\item Create the UBI volume for the root filesystem on the NAND flash:
\begin{verbatim}
zx_set_storage NAND
nand erase.part nand-rootfs
ubi part nand-rootfs
ubi create enzianbmc-zx5-rootfs -
\end{verbatim}
\item Update the device tree, Linux kernel and root filesystem images from the
TFTP server:
\begin{verbatim}
run update_devicetree
run update_kernel
run update_rootfs
\end{verbatim}
In each case confirm that no errors are reported.
\item Reset the BMC again, and verify that it boots to a Linux login prompt.
\item Watch for any errors or warnings during boot.
\item Log in (factory password is root/0penBmc).
\item At the Linux prompt, type:
\begin{verbatim}
ip addr show dev eth0
\end{verbatim}
Expect output like:
\begin{verbatim}
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast qlen 1000
    link/ether 0c:53:31:AA:BB:CC brd ff:ff:ff:ff:ff:ff
    inet 169.254.21.21/16 brd 169.254.255.255 scope link eth0
       valid_lft forever preferred_lft forever
    inet 192.168.1.XXX/24 brd 192.168.1.255 scope global dynamic eth0
       valid_lft 484sec preferred_lft 484sec
    inet6 fe80::e53:31ff:fe03:58/64 scope link
       valid_lft forever preferred_lft forever
\end{verbatim}
Verify that the MAC address (\verb+link/ether+) corresponds to the value set
in u-boot, and that the interface has been assigned a valid address on your
subnet.
\item Connect to the BMC with SSH (port 22, username and password as for
serial console), on the IP address reported in the previous step.
\item Verify that the CPU NOR flash is accessible: \verb+cat /proc/mtd+ should
show \\
\verb+mtd0: 01000000 00010000 "enzian-bdk"+ \\
as the first partition.
\item Start the power manager shell with
\verb+enzian-shell bringup+.
\item Enable the remaining power rails with the following current limits:

    \begin{tabular}{ccc}
        Rail & $I_\text{limit}$(A) & $I_\text{max}$(A) \\
        \hline
        \verb+12V_PSUP+ & $1.0$ & $0.2$ \\
        \verb+12V_CPU0_PSUP+ & $1.0$ & $0.1$ \\
        \verb+12V_CPU1_PSUP+ & $1.0$ & $0.1$ \\
        \verb+5VSB_PSUP+ & $5.0$ & $2.0$ \\
        \verb+5V_PSUP+ & $1.0$ & $0.4$ \\
        \verb+3V3_PSUP+ & $5.0$ & $10.0$ \\
    \end{tabular}

\item Verify that no current limits trip, and that the measured supply current
is no more than $I_\text{max}$.
\item Verify that both fans are spinning at low RPM.
\item Enter \verb+common_power_up()+ at the power manager console.
\item Verify that \verb+12V_CPU0_PSUP+, \verb+12V_CPU1_PSUP+, \verb+5V_PSUP+,
and \verb+3V3_PSUP+ report as within range.
\item Verify that both fans spin up momentarily, then return to a moderate
RPM.
\item Enter \verb+print_voltage_all()+ and verify that all sensor for PSU
voltages read within 5\% of the value measured at TP347, TP348, or TP349.
\item Enter \verb+print_temp_all()+ and verify that all reported shutdown
temperatures are $<20^\circ$C above ambient.  Record CPU and FPGA shutdown
temperatures.
\item Enter \verb+cpu_fan_on(100)+ and verify that the CPU fan
runs at high speed.
\item Enter \verb+power.read_device_monitor("fans", "FAN_0_RPM")+, and record the
reported RPM value for 100\% duty cycle.
\item Enter \verb+cpu_fan_on(40)+ and verify that the CPU fan
runs at moderate speed.
\item Enter \verb+power.read_device_monitor("fans", "FAN_0_RPM")+, and record the
reported RPM value for 40\% duty cycle.
\item Enter \verb+cpu_fan_on(0)+ and verify that the CPU fan
is still turning, albeit slowly.
\item Enter \verb+power.read_device_monitor("fans", "FAN_0_RPM")+, and record the
reported RPM value for minimum duty cycle.
\item Enter \verb+fpga_fan_on(100)+ and verify that the FPGA fan
runs at high speed.
\item Enter \verb+power.read_device_monitor("fans", "FAN_1_RPM")+, and record the
reported RPM value for 100\% duty cycle.
\item Enter \verb+fpga_fan_on(40)+ and verify that the FPGA fan
runs at moderate speed.
\item Enter \verb+power.read_device_monitor("fans", "FAN_1_RPM")+, and record the
reported RPM value for 40\% duty cycle.
\item Enter \verb+fpga_fan_on(0)+ and verify that the FPGA fan
is still turning, albeit slowly.
\item Enter \verb+power.read_device_monitor("fans", "FAN_1_RPM")+, and record the
reported RPM value for minimum duty cycle.
\end{enumerate}

\begin{table}
\centering
\begin{tabular}{c|cc}
Rail & $V_\text{min}$ & $V_\text{max}$ \\
\hline
\verb+12V_CPU0_PSUP+ & $11.42$ & $12.58$ \\
\verb+12V_CPU1_PSUP+ & $11.42$ & $12.58$ \\
\verb+5VSB_PSUP+ & $4.75$ & $5.25$ \\
\verb+5V_PSUP+ & $4.75$ & $5.25$ \\
\verb+3V3_PSUP+ & $3.14$ & $3.47$ \\
\end{tabular}
\caption{PSU voltage limits\label{t:psu_v_limits}}
\end{table}

\subsection{CPU Boot and Load Test} \label{s:manuf_test:cpu_boot_load}

CPU load tests are to be conducted with a current-limited supply.

\paragraph*{Requirements}
\begin{enumerate}
\item Adjustable current-limited supplies.
\item BDK NOR flash image for the CPU (\verb+bdk.bin+)
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Load the BDK NOR flash image to the BMC filesystem, e.g. by TFTP.
\item Program the CPU NOR flash from the BMC console with
\begin{verbatim}
flashcp -v bdk.bin /dev/mtd0
\end{verbatim}
\item Enter \verb+cpu_power_up()+ at the power console.
\item Verify that CPU power rails \verb+VDD_CORE+, \verb+0V9_VDD_OCT+,
\verb+1V5_VDD_OCT+, \verb+2V5_CPU13+, \verb+2V5_CPU24+, \verb+VDD_DDRCPU13+,
and \verb+VDD_DDRCPU24+ all report as within range.
\item Confirm that the on-board LEDs associated with the voltage rails are correctly lit.
\item Verify that the firmware console is visible over the USB UART interface.
Press \verb+B+ if necessary to interrupt boot.
\item Enter \verb+print_voltage_all()+ and verify that all CPU rail voltages
are within limits given in \ref{t:cpu_idle_limits} on all enabled monitors.
\item Verify that reported voltages are all with 5\% of the values measured at
TP347, TP348, or TP349.
\item Enter \verb+print_current_all()+ and verify that the idle current is
within the limits given in \ref{t:cpu_idle_limits}.
\item Enter \verb+cpu_fan_on(0)+.
\item Monitor CPU temperature with \verb+print_temp_all()+ and verify that it
stabilises at $<40^\circ$C above ambient.  Record CPU idle temperature at
minimum fan speed.
\item Record both idle voltage and idle current for \verb+VDD_CORE+,
\verb+0V9_VDD_OCT+, and \verb+1V5_VDD_OCT+.
\item Enter \verb+cpu_fan_on(40)+.
\item Wait until CPU temperature stabilises, and record CPU idle temperature
at 40\% duty cycle.
\item Enter \verb+cpu_fan_on(100)+.
\item Wait until CPU temperature stabilises, and record CPU idle temperature
at 100\% duty cycle.
\item \label{i::manuf_test:board_config}At the CPU console, enter \verb+S+ to enter setup.
\item Enter \verb+B+ to configure a board model number. This is the \verb+Name+ string from \ref{sec:mac_calculation}, e.g. \verb+enzian_v3+.
\item Enter \verb+R+ to configure a board revision. This should be etched onto the Enzian PCB, e.g. \verb+1.5c+ for revision \verb+1.5c+.
\item Enter \verb+S+ to configure a serial number. This is the enzian board serial number on its own, e.g. \verb+11+
\item Compute the CPU base MAC address according to the rules in
\ref{sec:mac_calculation}.  Bits 47--24 are the fixed Enzian OUI
\verb+0C:53:31+, and bits 4--0 are \verb+0b00000+, indicating the 
base of the \verb+40G Ethernet #0+ device.  Bits 23--16 encode the production series and 15--5 the board
serial number assigned earlier.
\item Enter \verb+M+ to configure the base mac address just computed.
\item Enter \verb+N+ to configure the number of mac addresses (8).
\item Enter \verb+W+ to save the configuration to flash.
\item At the CPU console, enter \verb+E+ to enter diagnostics, and select menu
item \verb+17+ (Power burn options).
\item Set power throttle level to 100\% and start the full-power burn.
\item Allow the burn to run for at least 5m, until both CPU temperature and
\verb+VDD_CORE+ current stabilise, monitoring temperature with
\verb+print_temp_all()+ and current with \verb+print_current_all()+.
\item Verify that peak CPU temperature is $<50^\circ$C above ambient.
\item Enter \verb+print_voltage_all()+ and verify that all CPU rail voltages
are within limits given in \ref{t:cpu_load_limits} on all enabled monitors.
\item Enter \verb+print_current_all()+ and verify that the load current is
within the limits given in \ref{t:cpu_load_limits}.
\item Record CPU load temperature at 100\% fan duty cycle.
\item Record both load voltage and load current for \verb+VDD_CORE+,
\verb+0V9_VDD_OCT+, and \verb+1V5_VDD_OCT+.
\item End the power burn, and once CPU temperature has dropped spin the fan
down with \verb+cpu_fan_on(40)+.
\item Enter \verb+cpu_power_down()+ at the power console.
\end{enumerate}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VDD_CORE+ & $0.91$ & $0.98$ & $40$ \\
\verb+0V9_VDD_OCT+ & $0.87$ & $0.93$ & $3.0$ \\
\verb+1V5_VDD_OCT+ & $1.45$ & $1.575$ & $1.0$ \\
\end{tabular}
\caption{CPU idle voltage and current limits\label{t:cpu_idle_limits}}
\end{table}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VDD_CORE+ & $0.91$ & $0.98$ & $120$ \\
\verb+0V9_VDD_OCT+ & $0.87$ & $0.93$ & $3.0$ \\
\verb+1V5_VDD_OCT+ & $1.45$ & $1.575$ & $1.0$ \\
\end{tabular}
\caption{CPU load voltage and current limits\label{t:cpu_load_limits}}
\end{table}

\subsection{FPGA Programming and Load Test} 
\label{s:manuf_test:fpga_boot_load}

FPGA load tests are to be conducted with a current-limited supply.

\paragraph*{Requirements}
\begin{enumerate}
\item Adjustable current-limited supply.
\item FPGA load test bitstream: \verb+power_burn.bit+ and
\verb+power_burn.ltx+.
		\netnote{These can be found on `enzian-build.ethz.ch` in `/storage/bistreams`}
\item Linux host with installed Xilinx toolchain (Lab Edition 2020.1 or
later).
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Configure JTAG bypass jumpers (J54, J61, J67, J73) such that both BMC
and FPGA are connected and all others are bypassed.
\item Enter \verb+fpga_power_up()+ at the power console.
\item Verify that FPGA power rails all report as within range.
\item Enter \verb+print_voltage_all()+ and verify that all FPGA rail voltages
are within limits given in \ref{t:fpga_idle_limits} on all enabled monitors.
\item Verify that reported voltages are all with 5\% of the values measured at
TP347, TP348, or TP349.
\item Confirm that the on-board LEDs associated with the voltage rails are correctly lit.
\item Enter \verb+print_current_all()+ and verify that the idle current is
within the limits given in \ref{t:fpga_idle_limits}.
\item Enter \verb+fpga_fan_on(0)+.
\item Monitor FPGA temperature with \verb+print_temp_all()+ and verify that it
stabilises at $<30^\circ$C above ambient.  Record FPGA idle temperature at
minimum fan speed.
\item Record both idle voltage and idle current for {\ttfamily VCCINT\_FPGA},
{\ttfamily VCCINTIO\_BRAM\_FPGA}, \texttt{VCC1V8\_FPGA}, \texttt{MGTAVCC\_FPGA},
and \texttt{MGTAVTT\_FPGA}, and idle voltage for \texttt{MGTVCCAUX\_L} and
\texttt{MGTVCCAUX\_R}.
\item Enter \verb+fpga_fan_on(40)+.
\item Wait until FPGA temperature stabilises, and record FPGA idle temperature
at 40\% duty cycle.
\item Enter \verb+fpga_fan_on(100)+.
\item Wait until FPGA temperature stabilises, and record FPGA idle temperature
at 100\% duty cycle.
\item With a USB cable connected to J81 (JTAG) from a Linux host, check that
the FPGA is now visible in the JTAG scan chain, alongside the BMC:
\begin{verbatim}
program_flash -jtagtargets
\end{verbatim}
On success, you should see output similar to:
\begin{verbatim}
JTAG chain configuration
--------------------------------------------------
1 Digilent XXXXXXXXXXXXX
        2    jsn-JTAG-SMT3-XXXXXXXXXXXXX-WWWWWWWW-0  \
                    (name xcvu9p  idcode WWWWWWWW)
        2    jsn-JTAG-SMT3-XXXXXXXXXXXXX-YYYYYYYY-0 \
                    (name arm_dap  idcode YYYYYYYY)
        3    jsn-JTAG-SMT3-XXXXXXXXXXXXX-ZZZZZZZZ-0 \
                    (name xc7z015 idcode ZZZZZZZZ)
\end{verbatim}
Digits \verb+WWWW...+ identify the main FPGA.
\item Using the Vivado Hardware Manger, connect to the JTAG target identified
in the previous step, and program the XCVU9P device with the
\verb+power_burn.bit+ bitstream, and the \verb+power_burn.ltx+ probes file.
\item You will be presented with a VIO (virtual I/O) display with one
configurable probe: \verb+enable[23:0]+.  Each bit of this probe enables the
clock to approximately one $24^\text{th}$ of the device's flip-flops.
\item Set the enable bits one-by-one to gradually increase load on the device,
monitoring FPGA temperature and \verb+VCCINT_FPGA+ current continuously.
Ensure that the current does not exceed the maximum listed in
\ref{t:fpga_load_limits}, and that FPGA temperature remains within $50^\circ$C
of ambient.

Expect an increase of around 4A on \verb+VCCINT_FPGA+ for each enabled bit.
\item Once the greatest allowed current is reached (do not exceed the limit),
allow the temperature to stabilise, and again check \verb+VCCINT_FPGA+ current
against \ref{t:fpga_load_limits}.
\item Record load temperature for 100\% fan duty cycle.
\item Record load voltage and current for all FPGA rails.
\item Set all control bits to zero, and verify that load current and
temperature return to idle values.
\item Spin the fan down with \verb+fpga_fan_on(40)+.
\item Enter \verb+fpga_power_down()+ at the power console.
\end{enumerate}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VCCINT_FPGA+ & $0.88$& $0.92$ & $6.0$ \\
\verb+VCCINTIO_BRAM_FPGA+ & $0.88$ & $0.92$ & $1.0$ \\
\verb+VCC1V8_FPGA+ & $1.71$ & $1.89$ & $3.0$ \\
\verb+MGTAVCC_FPGA+ & $0.86$ & $0.94$ & $0.1$ \\
\verb+MGTAVTT_FPGA+ & $1.17$ & $1.23$ & $0.1$ \\
\verb+MGTVCCAUX_L+ & $1.71$ & $1.89$ & - \\
\verb+MGTVCCAUX_R+ & $1.71$ & $1.89$ & - \\
\end{tabular}
\caption{FPGA idle voltage and current limits\label{t:fpga_idle_limits}}
\end{table}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VCCINT_FPGA+ & $0.88$& $0.92$ & $93$ \\
\verb+VCCINTIO_BRAM_FPGA+ & $0.88$ & $0.92$ & $1.0$ \\
\verb+VCC1V8_FPGA+ & $1.71$ & $1.89$ & $3.0$ \\
\verb+MGTAVCC_FPGA+ & $0.86$ & $0.94$ & $0.1$ \\
\verb+MGTAVTT_FPGA+ & $1.17$ & $1.23$ & $0.1$ \\
\verb+MGTVCCAUX_L+ & $1.71$ & $1.89$ & - \\
\verb+MGTVCCAUX_R+ & $1.71$ & $1.89$ & - \\
\end{tabular}
\caption{FPGA load voltage and current limits\label{t:fpga_load_limits}}
\end{table}

\subsection{DRAM Testing} \label{s:manuf_test:dram_testing}

\subsubsection{CPU DRAM Testing} 
\label{s:manuf_test:dram_testing:cpu}

CPU DRAM testing may be conducted using a standard SSI EEB-compatible (ATX24 + 2
8-pin CPU 12V connectors) power supply.

\paragraph*{Requirements}
\begin{enumerate}
\item 4 identical 32GiB ECC DDR4 RDIMMs, speed grade at least 2133, two ranks,
non-3DS.
\item BDK image already in CPU NOR flash.
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Insert all 4 DIMMs into the CPU DRAM sockets (\verb+CPU_DDR4_1$+ through
\verb+CPU_DDR4_4+).
\item Power the CPU up at the BMC console:
\begin{verbatim}
common_power_up(); cpu_power_up()
\end{verbatim}
\item Interrupt the CPU boot (CPU console) by pressing \verb+B+.
\item At the BMC console, verify that DDR idle voltages and currents are
within the ranges set by \ref{t:cpu_dram_idle_limits}.  Record the values.
\begin{verbatim}
print_voltage_all()
print_current_all()
\end{verbatim}
\item Press \verb+E+ to enter BDK diagnostics, and look for output similar to
the following, indicating that all 4 DIMMs are correctly recognised:
\begin{verbatim}
N0.LMC0.DIMM0: 16384 MB, DDR4 RDIMM 2Rx8 ECC, \
    p/n: 9965698-012.A00G, s/n: 808770835, 1.2V
N0.LMC0 Configuration Completed: 16384 MB
N0.LMC1.DIMM0: 16384 MB, DDR4 RDIMM 2Rx8 ECC, \
    p/n: 9965698-012.A00G, s/n: 825546771, 1.2V
N0.LMC1 Configuration Completed: 16384 MB
N0.LMC2.DIMM0: 16384 MB, DDR4 RDIMM 2Rx8 ECC, \
    p/n: 9965698-012.A00G, s/n: 758439187, 1.2V
N0.LMC2 Configuration Completed: 16384 MB
N0.LMC3.DIMM0: 16384 MB, DDR4 RDIMM 2Rx8 ECC, \
    p/n: 9965698-012.A00G, s/n: 808771859, 1.2V
N0.LMC3 Configuration Completed: 16384 MB
\end{verbatim}
		If you do not see output similar to this, configure the board information as in \ref{i::manuf:board_config}, reset the CPU and return here.
\item Navigate to the memory testing submenu (\verb+4+, \verb+4+).
\item Ensure that the test parameters are set as follows:
\begin{verbatim}
 1) Bringup Cores for multi-core testing (48)
 2) Number of times to repeat the test (1)
 3) Starting address (0x0)
 4) Length of the range to check (All)
\end{verbatim}
\item Set the CPU fan to 100\% (the CPU will be heavily loaded):
\begin{verbatim}
    cpu_fan_on(100)
\end{verbatim}
\item Press \verb+6+ to start the DRAM test.
\item Monitor the test progress, and verify that no errors are reported.
\item Record DRAM load voltages and currents, and verify that they are within
the limits given in \ref{t:cpu_dram_load_limits}.
\item On success, the test will terminate with output similar to:
\begin{verbatim}
All tests passed (time 01:16:14)
\end{verbatim}
\item Power the CPU down at the BMC console:
\begin{verbatim}
cpu_power_down(); common_power_down()
\end{verbatim}
\end{enumerate}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VDD_DDRCPU13+ & $1.14$& $1.26$ & $0.5$ \\
\verb+VDD_DDRCPU24+ & $1.14$& $1.26$ & $0.5$ \\
\verb+VTT_DDRCPU13+ & $0.57$& $0.63$ & - \\
\verb+VTT_DDRCPU24+ & $0.57$& $0.63$ & - \\
\end{tabular}
\caption{CPU DRAM idle voltage and current
limits\label{t:cpu_dram_idle_limits}}
\end{table}

\begin{table}
\centering
\begin{tabular}{c|ccc}
Rail & $V_\text{min}$ & $V_\text{max}$ & $I_\text{max}$ \\
\hline
\verb+VDD_DDRCPU13+ & $1.14$& $1.26$ & $7.0$ \\
\verb+VDD_DDRCPU24+ & $1.14$& $1.26$ & $7.0$ \\
\verb+VTT_DDRCPU13+ & $0.57$& $0.63$ & - \\
\verb+VTT_DDRCPU24+ & $0.57$& $0.63$ & - \\
\end{tabular}
\caption{CPU DRAM load voltage and current
limits\label{t:cpu_dram_load_limits}}
\end{table}

\subsubsection{FPGA DRAM Testing} 
\label{s:manuf_test:dram_testing:fpga}

FPGA DRAM testing may be conducted using a standard SSI EEB-compatible (ATX24 + 2
8-pin CPU 12V connectors) power supply.
The FPGA requires a different bitstream for different DIMMs. 
Currently, the FPGA is known to support 16 GiB DIMMs @ 2400 MHz, 128 GiB RDIMMs @ 2133 MHz, and 128 GiB LRDIMMs @ 2133 MHz.

\paragraph*{Requirements}
\begin{enumerate}
	\item 4 identical 16 GiB ECC DDR4 RDIMMs, speed grade at least 2400, two ranks, non-3DS.
	\item 4 identical 128 GiB ECC DDR4 RDIMMs, speed grade at least 2133, two ranks, non-3DS
	\item 4 identical 128 GiB ECC DDR LRDIMMs, speed grade at least 2133, two ranks
	\item Three memtest bitstreams
	\item Vivado and Vitis for running memory test
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
	\item Insert all 4 DIMMs into the FPGA DRAM sockets (\verb+FPGA_DDR4_1$+ through
	\verb+CPU_DDR4_4+).
	\item Power the FPGA up at the BMC console:
	\begin{verbatim}
	common_power_up(); fpga_power_up()
	\end{verbatim}
	\item Program the FPGA through Vivado using the corresponding bitstream. (TODO: more details, screenshot)
	\item At the BMC console, verify that DDR idle voltages and currents are
	within the ranges set by \ref{t:fpga_dram_idle_limits}.  Record the values.
	\begin{verbatim}
	print_voltage_all()
	print_current_all()
	\end{verbatim}
	\item Load Vitis and create project for Microblaze
	\item Run memtest on running Microblaze
	\item Confirm no errors through single iteration
\end{enumerate}

\subsection{Interface PRBS Testing}
\label{s:manuf_test:prbs_testing}

Signal integrity testing of high-speed interfaces (PCIe, ECI, Ethernet) is
conducted using PRBS (pseudo-random bitstream) generators and checkers, with
the physical interfaces looped back at the board boundard (e.g. QSFP cage or
PCIe edge connector).  The CPU- and FPGA-side testing can be conducted
independently, except for ECI, which connects to both.

\subsubsection{CPU Interfaces}
\label{s:manuf_test:prbs_testing:cpu}

The ThunderX CPU has built-in PRBS generators for all high-speed serial
interfaces, and testing is conducted by enabling these in the BDK.

\paragraph*{Requirements}
\begin{enumerate}
\item 3 NVMe loopback connectors (SFF-8643/SFF-8644) e.g. CS Electronics p/n
12G-HD-44LB.
\item 2 QSFP+ or QSFP28 loopback connectors e.g. Molex p/n 74763-0020.
\item 1 FMC loopback card e.g. Whizz Systems p/n WHZ-FMC XM-107\_6966 plus
SMA-SMA cables wired in a loopback configuration.
\item 1 x8 or greater PCIe loopback card e.g. Hitech Global p/n
HTG-PCIE-EP-SMA Rev.1.0.
\end{enumerate}

\paragraph*{Procedure}
\begin{enumerate}
\item Connect the NVMe loopback connectors to the CPU-side NVMe ports:
\verb+J27_1+, \verb+J27_2+, and \verb+J27_3+.
\item Connect the QSFP loopback connectors to the CPU-side 40GbE ports:
\verb%CPU_QSFP+_1% and \verb%CPU_QSFP+_2%.
\item Connect the PCIe loopback card to the CPU PCIe socket (x8 electrical,
x16 physical): \verb+J25+.
\item Connect the FMC loopback card to the FMC connector: \verb+J42+.  It may
be necessary to remove the standoffs, if fitted.
\item Power the CPU up at the BMC console:
\begin{verbatim}
common_power_up(); cpu_power_up()
\end{verbatim}
\item Interrupt the CPU boot (CPU console) by pressing \verb+B+.
\item Press \verb+D+ to enter BDK diagnostics.
\item Navigate to the SERDES configuration submenu (\verb+2+).
\item Verify that QLM (quad-lane SERDES module) settings are correct; They
should be as follows (reported reference clock may vary slightly):
\begin{verbatim}
 2) QLM 0 - 1 XLAUI, 4 lanes @10.312 GBaud, ref 156.248 Mhz
 3) QLM 1 - 1 XLAUI, 4 lanes @10.312 GBaud, ref 156.248 Mhz
 4) QLM 2 - 1 PCIe, 4 lanes @ 8.000 GBaud, ref  99.999 Mhz
 5) QLM 3 - 4 SATA, one lane each @ 6.000 GBaud, ref  99.999 Mhz
 6) QLM 4 - 1 PCIe, 8 lanes @ 8.000 GBaud, ref  99.999 Mhz
 7) QLM 5 - 1 PCIe, 8 lanes @ 8.000 GBaud, ref  99.999 Mhz
 8) QLM 6 - 1 PCIe, 4 lanes @ 8.000 GBaud, ref  99.999 Mhz
 9) QLM 7 - 1 PCIe, 4 lanes @ 8.000 GBaud, ref  99.999 Mhz
 10) QLM 8 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
 11) QLM 9 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
 12) QLM 10 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
 13) QLM 11 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
 14) QLM 12 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
 15) QLM 13 - Cavium Coherent Processor Interconnect @10.312 GBaud, ref 156.248 Mhz
\end{verbatim}
The QLM functions and connector assignments are listed in Table
\item Navigate to the PRBS and SERDES tuning submenu (\verb+16+).
\ref{t:cpu_qlm_assignment}.
\end{enumerate}

\begin{table}
\centering
\begin{tabular}{rlll}
QLM & Function & Line rate (Gbps) & Connector(s) \\
\hline
0 & QSFP+ 1 (40GbE) & 10.3125 & \verb%CPU_QSFP+_1%\\
1 & QSFP+ 2 (40GbE) & 10.3125 &  \verb%CPU_QSFP+_2%\\
2 & NVMe 1 & 8 & \verb+J27_1+ \\
3 & SATA 0--3 & 6 & \verb+J28+--\verb+J31+\\
4 & PCIe x8 lanes 0--3 & 8 & \verb+J25+ \\
5 & PCIe x8 lanes 4--7 & 8 & \verb+J25+ \\
6 & NVMe 2 & 8 & \verb+J27_2+ \\
7 & NVMe 3 & 8 & \verb+J27_3+ \\
8 & OCI0 - ECI link 0 lanes 0--3 & 10.3125 & - \\
9 & OCI1 - ECI link 0 lanes 4--7 & 10.3125 & - \\
10 & OCI2 - ECI link 0 lanes 8--11 & 10.3125 & - \\
11 & OCI3 - ECI link 1 lanes 0--3 & 10.3125 & - \\
12 & OCI4 - ECI link 1 lanes 4--7 & 10.3125 & - \\
13 & OCI5 - ECI link 1 lanes 8--11 & 10.3125 & - \\
\end{tabular}
\caption{CPU QLM assignement\label{t:cpu_qlm_assignment}}
\end{table}

\subsubsection{FPGA Interfaces}
\label{s:manuf_test:prbs_testing:fpga}


\subsubsection{ECI}
\label{s:manuf_test:prbs_testing:eci}


\subsection{Interface Functional Testing}
\label{s:manuf_test:int_func_test}
