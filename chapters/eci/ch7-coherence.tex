\chapter{Cache Coherence Layer}\label{sec:eci-cc}

There is a single physical address space shared between all nodes in an Enzian system.
One section of this space is dedicated for main memory access, handled by local DRAM controllers.
Cores can access any memory address through the local cache controllers.
These communicate with each other and guarantee coherent access for the entire memory address space to the cores.

ECI CC is the write-back sparse-directory cache coherence protocol which enables this.
It operates on the level 2 cache and is currently only defined for two nodes but it could be extended to more nodes in theory.

In either direction, ten VCs are allocated for ECI CC.
The logical endpoints are different sub modules of the cache controllers on either side of the ECI link.

This section offers a functional overview of ECI CC and describes how it is integrated into the rest of an Enzian system.
The perspective chosen is from a protocol endpoint implementors point of view but remains on the conceptual surface.

Intentionally omitted is a deep-dive into the details of all possible transactions and state transition.
This is already described in the \textit{ECI Cache Coherence Protocol Specification}, a result from Nikita's master's thesis.

Furthermore, a machine readable specification has been produced as part of Jakob's master's thesis, in the form of a bunch of YAML files. The state transitions are defined in \texttt{directory\_controller.yaml} and \texttt{l2\_controller.yaml}.

\section{Directory Protocol Terminology}
In a directory-based cache coherence protocol, each cache line has an assigned home node.
In an Enzian system, the home node for a cache line is determined by the physical location of the corresponding DRAM.
In terms of addressing, two bits in the physical address are reserved for the node identifier, which reflects the home node for a specific address.

We will refer to the node which is not the home node as remote node and to cache lines which are not managed by the local directory controller as remote cache lines.

For an ongoing request, we also define the roles of the requester node and the owner node.
The requesting node is simply the node which has initiated a request.
The owner node on the other hand is the node currently holding the cache line.
Both of these roles can be hold by either the home node or the remote node.

\section{The Enzian Cache}
ECI CC assumes that there is a cache system available on either side of the link, which we call the Enzian cache.

To get something working in the first place, it should be possible to correctly participate in ECI CC without any actual local cache.
But in that case, all caching responses for remote addresses would have to be followed by an immediate invalidation.
This is contrary to the Enzian cache design and is not how ECI CC is intended to be used effectively.

The Enzian cache uses 128-byte sized cache lines.
8192 cache sets are assumed to exists, addressable with 13-bit indices.
Having at least one cache line in a set, a minimal cache size of 1 MB can be derived.

The L2 cache of a ThunderX node, in some sense, acts as reference implementation for an Enzian cache.
It uses 16-way associate cache sets, hence the total cache is 16 MB large.

Since the degree of associativity is immaterial for the ECI CC protocol, implementor can investigate different design options.
It is even possible to abandon the classical design with static, uniform cache sets.
For example, we could have different cache set sizes for different address ranges, using prior knowledge about the applications that will use it.
Or the cache set sizes could change dynamically in response to runtime demand.

For the cache tag, all bits which are not part of the index should be used.
The cache at this level is physically indexed and physically tagged (PIPT).

\subsection{Cache Set Selection}\label{sec:eci-cc-index}

Two indexing modes exist to translate addresses to cache set indices.
The first mode uses bit 19 to 7 of the physical address and is the default.

The second mode is called \textit{aliased-mode}, defined in detail in the ThunderX hardware reference manual.
It uses the same bits as input but additionally mixes them up.
The goal of the resulting hash function is to avoid performance degradation on certain memory access patterns.

If the aliased-mode is used, it must be configured like this on all nodes, since some field of ECI CC depend on the cache line indices.

\subsection{Cache Line States}

ECI CC uses the four major states \texttt{OWNED}, \texttt{EXCLUSIVE}, \texttt{SHARED}, and \texttt{INVALID}.
In the \texttt{OWNED} state, an additional dirty flag is used, effectively adding another state which would be called \texttt{MODIFIED} in other protocols but we usually treat the dirty flag as independent of the major states.

\textbf{Ownership} of a cache line is defined as having a cache line in either the \texttt{OWNED} or \texttt{EXCLUSIVE} state (independent of dirty flag).

\section{Message Categories}\label{sec:eci-cc-categories}

To implement ECI CC, there is set of defined message types.
They are distinguished by the message command field, which is shared among all ECI CC messages (and indeed also ECI IO messages).

In general, each message type falls into one of the following categories.\footnote{There is one exception: \texttt{VICDHI} covers the intent of both forwarding response and an eviction.}

\begin{itemize}
    \item \textbf{Remote memory requests and responses}
    \item \textbf{Forwarding requests and responses}
    \item \textbf{Eviction notifications}
    \item \textbf{Atomic operations and responses}
\end{itemize}

This list of categories has been compiled to order messages by their intent.
Each category is elaborated below to give a high-level overview of the ECI CC protocol.

These categories are orthogonal to VC usage as defined in table \ref{table:vcs}, which is split by requests and responses. (Evictions are treated like responses.)

\subsection{Remote Memory Requests and Responses}
When a node accesses a \textbf{remote cache line} which it does not have cached already, it will issue a memory request to the home node.
A memory response with the requested data will be returned.
It is in the responsibility of the home node to stall the request until it can be served.

\subsection{Forwarding Requests and Responses}
When a node accesses a \textbf{home cache line} that is currently owned by the remote node, it will issue a memory forward request to the remote. This will be answered by a forward response.

This case is actually very similar to remote memory requests, with the difference that the home node is now requester instead of owner.

The term \textit{Forward} in this context comes from more general directory-based protocols.
In the Enzian cache, only two nodes exists.
Hence, the only possible case for a request at the home directory which cannot be served locally is the one just described.

In a more general setting, with more than two nodes, every remote access to a cache line (not locally cached already) causes a request to the home directory controller.
The home node might be able to answer it directly or otherwise has to forward it to another remote node.

A forwarded request will then be answered by a forward response directly from the remote to the requester.
To allow the home node to serialize all requests, an additional acknowledgment is also sent back to home, if it is not also the requester.

None of this is necessary in an Enzian system but the naming conventions for the messages reflect this more general protocol.

A special case of a forward request, which does appear in Enzian, is a forced eviction, which is issued when the directory controller decides to evict the entry from the sparse directory.

\subsection{Eviction Notifications}
Another type of messages is used when a remote node evicts a cache line, presumably to replace it with another cache line.
The directory controller is notified explicitly about this event, to keep it up to date and to free up space in the sparse directory.

\subsection{Atomic Operations}
ECI CC supports several atomic operations, such as \textit{conditional store} and \textit{compare-and-swap}.
The other atomic operations are of arithmetic kind.

For example, \texttt{RINC} is an atomic increase operation which is executed in remotely cached data.
The expected answer, according to our current specification, is a message of type \texttt{PATM}.
However, this message category has not yet been studied.
This means that our specification on this is in a speculative state and no efforts to verify it against the hardware have been undertaken.

\subsection{General Notes on ECI CC Messages}
For most of the message categories, there are many message types with slightly different intentions.
For example, an eviction message can be from a shared state to invalid (\texttt{VICS}), or from owned to invalid (\texttt{VICD}).
Using such specific message types allows for an efficient event-based cache controller implementation, which does not have to look up the current state of the cache line in the directory to process the request.

Similar specific message types also exist for memory requests and forward requests.

\section{Cache Controllers}

The cache controller in its most general sense refers to the subsystem on one node which handles all cache requests, including both local and ECI CC requests.

The internal structure of the cache controller is not just an implementation detail but essential for the specification of ECI.
Certain components of the cache controller are addressed by header fields on ECI CC messages.
Hence, to understand ECI CC messages, we need to assume a couple of things about the cache controller implementation.

\subsection{Reference Cache Controller Implementation}\label{sec:eci-l2c}

The ThunderX's cache controller components are explained below, representing a reference implementation for an Enzian cache.

A ThunderX is coherent up to and including the level 1 data cache.
Each core has its own level 1 data cache and hardware guarantees for coherent access by software.
The point of unification is at the level 2 cache, which is the main subject of this section.

The L2C is connected to the cores through the Coherent Memory Interconnect (CMI).
The CCPI interface controller (also known as ECI interface), is directly attached to the CMI, alongside with the IO bridges.

The ThunderX L2C hierarchy is depicted in figure \ref{fig:l2structure}.
At the highest level, the L2 cache is divided in smaller 8 components, called tag and data unit (TAD).
Each of them has full authority over a subset of the L2 cache.
This subset is then further divided into two Quad Groups per TAD, four Quads per Quad Group, and eventually, into 128 16-way cache-sets per Quad.

From a cache placement policy view, each Quad Group (and each Quad) manages a unique range of cache sets with contiguous indices.
This structure will be important for section \ref{sec:eci-transaction} about transaction control.
Specifically, when it comes to routing responses to the component awaiting it, without knowing the full memory address.

\begin{figure}
    \centering
    \def\svgwidth{\columnwidth}
        \input{chapters/eci/figs/l2-structure.pdf_tex}
    \caption{Overview of the L2 cache structure on one node}
    \label{fig:l2structure}
\end{figure}

\subsubsection{Address and Data Buffers}\label{sec:eci-buffers}
While a cache controller handles a request, it stores the request (including its address) in an in-flight address buffer (LFB).
In association with each LFB, there is also a victim address buffer (VAB), tracking blocks being written to DRAM.

To hold the actual data, there are three more types of buffers.
Fill buffers (FBF) for data read from the L2 cache or DRAM, store buffers (SBF), for all store operations, and victim data buffers (VDB), which are used to write data to DRAM.

The availability of buffers is a potential bottleneck.
Thus, the buffer allocation at the Quad Group level is subject to priority queueing and is affected by quality of service configurations.

On a ThunderX, there are 16 LFBs per Quad Group available and they are the limiting factor or how many transactions can be open at the time.
Before allocating an LFB for a transaction, the transaction is placed in one of eight quality of service queues.
When an LFB is available, a transaction from one of the queues will be popped.
Before that, no processing takes place for the transaction.

The strategy to decide which queue to pop a transactions from can be configured.
More details on this can be found in the hardware reference manual on page 169.

The LFB stays occupied by a transaction until it has completed, which may include DRAM access or waiting for an ECI CC response.
The data buffers are allocated only afterwards and independently, which may delay a transaction and hence the time an LFB is occupied.

A transaction can also time out, in which case it will release the buffers and abort the transaction.

\subsubsection{Directory Controller}
The directory controller keeps an entry for all home cache lines which are currently present (in shared or owned form) on the remote node.
One entry contains the state of the cache line on both nodes, for example local \texttt{I} and remote \texttt{S}.

On the ThunderX, the sparse directory is placed on-chip in the L2C.
Therefore, the directory and the L2 cache can be efficiently queried in parallel upon receiving new requests.
But this is in no means necessary and it would be possible to have the directory controller as separate components.

\section{Transaction Lifecycle}\label{sec:eci-transaction}

An Enzian node can have up to 512 open transactions waiting for an ECI CC response at any given moment.
More than this cannot be uniquely identified by the header fields.

On a ThunderX, these 512 transactions correspond to eight TADs with two Quad Groups and 16 buffers each.
Other designs are possible for the FPGA node with a different structure, keeping the limit of 512 buffers in mind.

To explain how these buffers are used and identified, the following shows the entire lifecycle of an ECI CC transaction for a remote load.

\subsection{Remote Load Example: Start of Transaction}
Suppose the L2C starts processing a load request for a remote address, triggered by a request from a core.
An LFB is allocated for this purpose, which implicitly opens a transaction.

The first step will be to query the L2 cache, which we assume does not hold a sufficient entry.

Since the request cannot be served locally, the L2C issues an ECI CC request.
The request contains the address of the cache line in question but also a transaction identifier.
The transaction identifier has to be unique for an allocated LFB within a TAD, for reasons to be explained soon.
This identifier goes into the remote requester ID field.

\subsection{Remote Load Example: Answering a Request}\label{sec:msg-req-routing}

When the memory request reaches the other node, it must be assigned to the correct L2C component.

For this, the cache set index is derived form the address as described in \ref{sec:eci-cc-index}.
The cache set index then dictates which Quad Group in which TAD is responsible for handling this request.

In a two node system, the cache line at this point cannot be owned by a remote node.
Thus, the Quad Group is able to handle the request locally.
Either by finding a valid entry in the corresponding cache set, or loading the data from DRAM.
A response is generated accordingly.

In the response header, the transaction identifier from the request hast to be provided again.
From the address, only the TAD index is included, which is part of the previously derived cache set index.

\subsection{Remote Load Example: Loading a Pending Transaction}\label{sec:msg-rsp-routing}

Back on the other side, the response will be received without an address field, potentially out-of-order with other simultaneous transactions.

The message has to be assigned to the quad group which has opened the request.
Only after that step can we recover the cache line address, which is stored in one of the LFBs.

Since the TAD is included in the header, the first step of \textit{routing} to a TAD is easy.
Afterwards, we only have the transaction identifier.

At this points, we can see that transaction identifiers really must be unique within a TAD, even though they are allocated per quad group, which is only half a TAD.
But given that they are indeed unique within a TAD, we can find out which Quad Group must have issued the request and forward the response to it.
The Quad Group then has access to all associated buffers and can finish the transaction.

\section{Home Transactions vs Remote Transactions}
Two fields with different names exist for transaction identifiers on ECI messages.
Depending on whether the requester is the home node or the remote node with respect to the address,
either the \textit{home requester id}
or the \textit{remote requester id} field is used.

In other words, home request ids are used for forwards and remote request ids for remote requests.

The remote requester id corresponds to an LFB of the L2C, as discussed in the remote load example.
These are for pending transactions to be finished by the L2C.

The home request id, on the other hand, corresponds to an open transaction in the directory controller.
They are independent of the LFBs maintained by quad groups.
But there has to be an equivalent address buffer for them, since the responses to forwards do not contain addresses, either.
It also seems that there are twice as many buffers available, given that the home transaction identifier field has one bit more than the remote transaction identifier.

At the time of writing, we have not quite figured out, yet, how the home request ids are managed by a ThunderX internally.
But in terms of what is visible on CCPI traffic, trace analysis yields that the ids are unique per TAD, just like the remote transaction ids.
Consequently, they can be handled exactly like request ids.

For now, we assume there are 16 remote LFBs and 32 home LFBs available per quad group, since this model works with all traces we have seen so far. This assumption is also integrated into figure \ref{fig:l2structure}.
