
\chapter{ECI Block Layer}

\section{Block Layer Communication Protocol}

\subsection{A Virtual Channel}
The block layer communication protocol defines the concept of a \textit{virtual channel} (VC) as a communication conduit for a certain message type, hence different message types will use different virtual channels. This separation and allocation of multiple virtual channels allow the link partners to exploit the full bandwidth of the link. It also isolate traffic control of each message type preventing one message type from thrashing and blocking the link for other message types. The block layer allocates 14 virtual channels (numbered VC0 -- VC13) in total which will be explained in section~\ref{sec:vcs}. 

\subsection{The Block Data Structure}
The block layer communication protocol defines a data structure of 64-bytes (called \textit{Block}) as a carrier that encapsulates messages of different virtual channels between the ThunderX and FPGA. A Block is partitioned into eight 64-bit \textit{Words}, one of them is used as a Block Header and the remaining seven Words are used to carry actual messages data.
There are three different types of Blocks: \textit{Synchronization Blocks, Data/Credit Blocks, and Idle Blocks.} 

\subsection{Synchronization Block}
Synchronization blocks are exchanged between two nodes (i.e., ThunderX and FPGA) during the setup of the link to sync the link state machines on both nodes. The Sync block header includes the following fields:
\begin{itemize}
	\item CRC[23:0]: This field is 24-bits from bits-0 to bit-23. The CRC field exists in all block types as it is the only way to detect errors in the block.
	\item RXSEQ[31:24]: This is an 8-bit counter of the number of received data/credit blocks without errors. 
	\item TXSEQ[39:32]: This is an 8-bit counter of the number of re-played data/credit blocks.
	\item Req[53:52]: These 2-bits field indicate the request type. "01" is for link initiation request, and "11" is for link replay request which is sent (or received) in case an erroneous block is detected.
	\item InitAck or ReqAck[60]: This is an acknowledgement bit for either initiation request (Req[53] = '0') or a replay request (Req[53] = '1').
	\item SYNC[63:61]: This is a 3-bit opcode indicates the block type. For a sync block the opcode is "110".
\end{itemize}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.67]{chapters/eci/figs/sync}%
	\caption{Sync Block format. All data words are unused.}
	\label{fig:sync}
\end{figure}

\subsection{Data/Credit Block}

Figure~\ref{fig:dblock} depicts the format of a block carrying messages data or returned/sent credits from/to the link partner. In addition to the CRC field, a data/credit block header includes the following fields:
\begin{itemize}
	\item VCs[51:24]: This field contains 7 distinct 4-bit subfields each corresponds to a data word in the block (check the Figure~\ref{fig:dblock} for more details). If there is a valid data word in the block then the corresponding VC field value is in the range 0 -- 13. If a data word does not carry any data, then the corresponding VC value is 15. 
	\item CREDITs[59:52]: Each bit corresponds to a one credit point returned (or sent) to the corresponding VC. A one credit point is worth 8 data words. If the block opcode is "LO" (in bits[63:61]) then the 8 credit bits correspond to [VC7:VC0]. For block opcode "HI" only the first 5 bits are used for [VC12:VC8], the remaining 3 bits are set to "3'b000". 
	\item ACK[60]: This bit acknowledges that the link partner received 8 error-free data/credit blocks.
	\item HI or LO[63:61]: This is a 3-bit opcode indicates the block type. It is either "3'b100" for a LO credit block, or "3'b101" for HI credit blocks. 
\end{itemize}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.67]{chapters/eci/figs/dblock}%
	\caption{Data Block format.}
	\label{fig:dblock}
\end{figure}

\subsection{Idle Block}
When there are no data/credit blocks to exchange between the two nodes, and the link is not in a synchronization state, Idle blocks must be continuously exchanged between the two nodes to keep the link synced and stable. The Idle block allows us to return block acknowledgement, "ACK" bit in Figure~\ref{fig:idle}. It carries also the "TXSEQ" and "RXSEQ" counters as in the Sync block. It does not carry any VC credit and data words are unused.

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.67]{chapters/eci/figs/idle}%
	\caption{Idle Block format.}
	\label{fig:idle}
\end{figure}

\subsection{Block Layer  State-Machine}
\label{sec:state-machine}
The block layer operation is controlled by a state-machine that dictates how it is setup, and how to handle detected errors.  Figure~\ref{fig:fsm} sketches the state-machine of block layer. 

\subsubsection{IREQ State:} 
\begin{itemize}
	\item \textit{Entering this State:} The state-machine enters this state at cold reset, or when the link partner sends an initiation request (Req[53:52] = "01" in the received Sync block) from whichever state the link exists in except the IACK state. Also if the RREQ state times out, then the link enters the IREQ state.
	\item \textit{Exiting this State:} The state-machine leaves this state only when it receives an initiation request acknowledgement from the link partner (InitAck bit is set to '1' in received Sync blocks and Req[53] = '0'). From this state the state-machine goes to the IACK state.
	\item \textit{Action in this State:} The state-machine generates Sync blocks with the field Req[53:52] = "01" continuously until it exits this state. At the same time, the state-machine generates continuously acknowledgements for received initiation requests from the link partner until it stops receiving such initiation requests.
\end{itemize}

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.7]{chapters/eci/figs/state-machine}%
	\caption{Block layer state machine.}
	\label{fig:fsm}
\end{figure}

\subsubsection{IACK State:} 
\begin{itemize}
	\item \textit{Entering this State:} The state-machine enters this state when it leaves the IREQ state.
	\item \textit{Exiting this State:} The link leaves this state when an erroneous Sync block is received (CRC error) and goes to the RREQ state. It also leaves this state, when the link partner stops sending initiation requests and acknowledgements. 
	\item \textit{Action in this State:} While in this state, the state-machine generates continuously Sync blocks with initiation request acknowledgements if it is receiving initiation requests. Otherwise, it just generates Sync blocks with no requests or acknowledgements.
\end{itemize}

\subsubsection{RUN State:} 
\begin{itemize}
	\item \textit{Entering this State:} The link enters this state when it leaves the IACK state. It also enters this state when it successfully executes a REPLAY operation or receive acknowledgements for its Retry Requests (will be explained later).
	\item \textit{Exiting this State:} The link exits this state if it receives a CRC error, or the link partner sends a retry requests because it received blocks with CRC errors (MAY BE there are other error reasons, NOT KNOWN YET).
	\item \textit{Action in this State:} This state is where actually the link is ready for exchanging data blocks between the link partners. The state-machine will generate data blocks if either there is credit points to be returned or there is valid VC data words to be transferred or both. Otherwise, it will generate IDLE blocks.
\end{itemize}

\subsubsection{RREQ State:} 
\begin{itemize}
	\item \textit{Entering this State:} The link enters this state if a block with a CRC error is detected. The link enters this state from any other state excepts IREQ and RACK states.
	\item \textit{Exiting this State:} The link exits this state once it receives a Sync block with Retry Request acknowledgement (ReqAck[60] = '1' AND Req[53] = '1').
	\item \textit{Action in this State:} In this state the link generates continuously Sync blocks with Retry Request (Req[53:52] = '11'). If it happens that it receives as well retry requests from its link partner (it also detected errors!) then it sets the ReqAck[60] bit to '1' to send acknowledgements. At the same time, a retry request timer starts counting, if it reaches the timeout limit before receiving retry request acknowledgments from the link partner, the links goes back to the IREQ state. Once the link receives acknowledgement for its retry requests, it exits this state and enters RACK state.
\end{itemize}

\subsubsection{RACK State:} 
\begin{itemize}
	\item \textit{Entering this State:} The link enters this state in 3 different cases: (1) It receives a retry request while in the RUN state. (2) It receives retry-request acknowledgement while in the RREQ state. (3) The link receives a retry-request while in the REPLAY state.  
	\item \textit{Exiting this State:} There are two different destinations for the link after this state: (1) The link exits this state toward the RUN state if it initially came to this state from the RREQ state and no "non-served" retry-request from the partner is sent at any point in time before this state or while in this state. The move to the RUN state occurs when it is no longer receiving retry acknowledgements and the link partner does not  send any retry-requests. (2) The link exits this state towards the REPLAY state if it has "non-served" retry-requests from the partner either received before entering this state or while in this state. The move occurs to the REPLAY state when the link partner does stops sending retry-requests or acknowledgements. 
	\item \textit{Action in this State:} If the link receives while in this state retry-requests from the link partner, it must send back retry-request acknowledgements until the link partner stops sending retry-requests. The link should set a PREV-RETRY-REQUEST flag (if not set yet) to decide toward which state to exit this state (i.e., toward REPLAY or RUN states). If the link detects a CRC error in this state on received blocks, it should store in a flag this detected error and NOT jump to the RREQ state. This error flag will be used later either in the RUN or REPLAY states to jump to the RREQ states.
\end{itemize}

\subsubsection{REPLAY State:} 
\begin{itemize}
	\item \textit{Entering this State:} The link enters this state if there is a pending received retry-request from the link partner. The link comes from the RACK state as described before.
	\item \textit{Exiting this State:} The link exits this state typically when it plays the last buffered block and jumps afterwards to the RUN state. It also exits this state if it detects a link error (e.g., CRC error) and jumps to the RREQ state. 
	\item \textit{Action in this State:} During this state, the link replays the a subset of previously sent data/credit blocks, already stored in the REPLAY buffer. The link uses the RXSEQ counter sent with the retry-request from the link partner as it indicates the ID of the last block the link partner received correctly.
\end{itemize}

\subsection{Virtual Channels}
\label{sec:block-vcs}
% What are the different VCs and how they communicate
The block layer communication protocol defines different virtual communication channels  for each message type. A virtual channel (VC) carries messages in the Block packets, it has its own traffic control and buffering. There are 7 types of messages:

\begin{itemize}
	\item \textit{Request with Data}: Messages with a request to move data from one node to another such as memory writes are communicated on this channel type. The amount of data carried by the request is variable and encoded within the request header.  The Block layer protocol allocates two channels of this type one on each memory bus.
	\item \textit{Request without Data}: Messages with a request that does not carry data such as requesting to read data from remote memory are communicated on these channels. There are also two channels of this type.
	\item \textit{Response with Data}: This channel type sends a response data for received requests without data (e.g., response to a read request). The amount of data to be sent back by the response is variable and encoded within the corresponding request header. The are two channels of this type.
	\item \textit{Response without Data}: Responses such as write acknowledgements carry no data and move on these channels. There are two channels of this type.
	\item \textit{Forward without Data}: Messages that should be forwarded from the receiving node to another one without carrying data will use these channel. There are two channels of this type.
	\item \textit{I/O Request}: This channel type carries general I/O requests destined for user defined registers are carried on these channels.  I/O requests can either carry data or not, such as write/read a register. There is one channel of this type.
	\item \textit{I/O Response}: A response to an I/O request either a write acknowledgement or read data response is carried on this channel. Typical data width on these channel I/O channels is 64/128-bit.  There is one channel of this type.
\end{itemize}

The virtual channels in the list above are used to communicate user level messages between the two nodes. There are 12 channels numbered VC0 -- VC11 for user level messages. 
In addition to these channels, the Block layer protocol allocates 3 other virtual channels dedicated for link control and setup messages communicated between the controller (ThunderX) and responder nodes (FPGA).

\begin{itemize}
	\item \textit{RSL I/O Request}: This channel carries I/O requests accessing  link control status registers (CSRs). Responses to these requests are sent back on the I/O Response channel. This channel share VC0 together with the General I/O Request messages, because the messages are also register read/write requests but to a separate link-control register file. 
	 \item \textit{Multiplexed Co-processor}: This channel is used to exchange messages between co-processors on the two nodes. The messages on this channel are formatted according to a specific  co-processor-communication protocol. (NOT MUCH KNOWN ABOUT IT!)	 
	 \item \textit{MCD / Link Data}: This channel is used to carry unique link data word. The purpose and operation of this channel is not very clear.
\end{itemize}

Note that every virtual channel can be in either bus direction: RX bus for received messages and TX bus for sent messages. Table~\ref{table:vcs} summarizes the VCs of the Block layer protocol.
\begin{table}
	\centering
\begin{tabular}{|c|c|c|c|}
	\hline 
	\textbf{VC ID} & \textbf{Message Type} & \textbf{Credit} & \textbf{BUS}  \\ 
	\hline 
	\hline 
	VC0 & General or RSL  I/O Request & 256 & RSL and GPIO \\ 
	\hline 
	VC1 & General or RSL I/O Response & 256 & RSL and GPIO  \\ 
	\hline 
	VC2 & Request with Data & 256 &  Memory Bus\#1  \\ 
	\hline 
	VC3 & Request with Data & 256 &  Memory Bus\#0  \\ 
	\hline 
	VC4 & Response with Data & 256 &  Memory Bus\#1  \\ 
	\hline 
	VC5 & Response with Data & 256 &  Memory Bus\#0  \\ 
	\hline 
	VC6 & Request w/o Data & 32 &  Memory Bus\#1  \\ 
	\hline 
	VC7 &  Request w/o Data & 32 &  Memory Bus\#0  \\ 
	\hline 
	VC8 & Forward w/o Data & 32 &  Memory Bus\#1  \\ 
	\hline 
	VC9 & Forward w/o Data & 32 &  Memory Bus\#0 \\ 
	\hline 
	VC10 & Response w/o Data & 32 & Memory Bus\#1  \\ 
	\hline 
	VC11 & Response w/o Data & 32 & Memory Bus\#0  \\ 
	\hline 
	VC12 & Multiplexed Co-processors  & 32 &  Co-processor Bus  \\ 
	\hline 
	VC13 & Multicore Debug, CSR Data & N/A &  MCD and Link Data  \\ 
	\hline 
\end{tabular}  
\caption{Virtual links summary and credits. One credit corresponds to one 64-bit data word.}
\label{table:vcs}
\end{table}

\subsection{VC Credit System}
% 
In an asynchronous communication link there is  a need to synchronize the two link partners to prevent any data loss because of a buffer overflow. A typical synchronization method is a credit-based system. The example in Figure~\ref{fig:credit-sys} helps to understand how a credit-based system works.

Consider nodes \textbf{A} and \textbf{B} on the link, each allocates a buffer space to store received data words. In addition, each node allocates a counter initialized with the buffer space of the other link partner.
When a node such as \textbf{A} sends data words to node \textbf{B}, it decrements the buffer space counter (i.e., \textbf{Counter\_B}). Node \textbf{A} sends data words as long as \textbf{Counter\_B}) is larger than zero. When node \textbf{B} consumes data words from its buffer (\textbf{Buffer B}), it informs node \textbf{A} of this new empty space, then node \textbf{A} increments \textbf{Counter\_B}. 
The credit bits [59:52] in the header word of the block are used by a node to inform the link partner of such newly freed buffer space.
Table~\ref{table:vcs} lists the amount of credit allocated for every virtual channel.

\begin{figure}[t]
	\centering
	\includegraphics[scale=0.5]{chapters/eci/figs/credit_sys}%
	\caption{Operation of a credit systems.}
	\label{fig:credit-sys}
\end{figure}

\subsection{Link Setup Flow}
% How the link is established? What messages/blocks exchanged during link setup
The link setup process goes through 3 main steps:
\begin{itemize}
	\item Link synchronization. In this step, the FPGA and ThunderX exchange synchronization blocks to initiate and acknowledge requests until both reach and stabilize in the RUN state. 
	\item Credit initialization. Upon first reaching the RUN state, the FPGA (and ThunderX) exchange the amount of credits (indicating their buffer space) of each virtual channel. The credit blocks are used to transfer the amount of credits of each VC. 
	\item Link Validation. Once the credits are exchanged, the ThunderX proceeds with a sequence of RSL registers read/write operations to verify the functionality and validity of the link.
\end{itemize}

The link setup operation is started in the BDK by invoking the function \textit{bdk\_boot\_ccpi()} (should be called \textit{bdk\_boot\_eci}) in apps/init/app.c file. Following on the detailed steps in the function hierarchy one can understand and know what CSR register read/write operations are exchanged with the FPGA. For now, only a limited number of these read/write operations are supported to have the link functional. 

\section{Architecture and Implementation}
Figure~\ref{fig:block-layer-arch} depicts the implemented circuit of the block layer. The circuit is implemented in SystemVerilog and located in enzian repository at "enzian/fpga/src/eci/eci\_link/block\_layer".
\begin{figure}[t]
	\centering
	\includegraphics[scale=0.4]{chapters/eci/figs/eci_arch}%
	\caption{Block Layer architecture.}
	\label{fig:block-layer-arch}
\end{figure}

\subsection{Link Receiver module (RLK)}
The receiver module "Link RLK" (located in link\_rlk.sv) implements the circuitry to process an incoming Block and outputs individual VC ports to the Virtual Channel Layer. The processing of received blocks passes through three steps: (1) Decoding incoming block types to decide what action to be done. (2) For blocks that hold valid VC words, it decodes to which VC these words belong and on which VC bus to put them. (3) Since words on a VC bus might first have gabs between them and not aligned, they are aligned first and the number of words is counted before putting them on the VC port.

\subsubsection{Block Decoder.} 
The \textit{Block Decoder} module separates the incoming blocks stream into 2 data streams: synchronization blocks stream forwarded to the Link State Machine, and data/credit blocks stream forwarded toward the VCs Decoder module. IDLE Blocks are dropped.  The Block Decoder only permits blocks with no CRC errors. In addition to the Sync Block passed to the link state machine, two more signals are forwarded:
\begin{itemize}
	\item "rx\_blk\_received" informs the state machine module of a correctly received data/credit block. 
	\item "rx\_blk\_error" informs the state machine module of a detected CRC error.
\end{itemize} 

The Block Decoder extracts the returned credit fields from the Block header and forward them to the Link Transmitter FIFOs (as will be explained later).

\subsubsection{VCs Decoder.}
Because every VC has its own bus, which is 448-bits wide (7 64-bit words). The VCs Decoder extracts the words of each VC from the block and put them on the VC bus. It also aligns them to the start of the bus, and eliminates gabs between words. For example, Let us assume there are three words for VC4 in the Block: words 3, 5, and 6. Then, the VCs Decoder first extracts them from the Block, then shifts them such that they occupy the bits [191:0] ordered as [W6,W5,W3] on the VC4 bus. 
The VC bus interface consists of three signals: Data (448-bits), Size (3-bits), and Valid (1-bit).

\subsubsection{VCs FIFOs.} 
Once the VCs data words are decoded and aligned, they are inserted into dedicated FIFOs for each VC. The FIFOs are the buffer space with the credit points for each VC. A FIFO is 448-bits wide holding up to seven data words, although for many VCs these width will not be used. When a data line in the FIFO is popped, the number of popped words in the data line is reported to the link state machine, which will return it back to the ThunderX. There are 14 VC FIFOs for all VCs. 
\subsection{Link Transmitter module (TLK)}
The Link Transmitter module (Link TLK) exposes 14 VC interfaces to the Virtual Layer, where user generated requests and link control messages are inputted to the link to be transmitted to the ThunderX. The Link TLK is located in link\_tlk.sv in the Enzian repository. The TLK performs three main tasks:
\begin{itemize}
	\item \textit{Monitor VC Credit available on ThunderX.} The TLK allocates FIFOs for the 14 VCs to store incoming user requests/responses before sending them to the ThunderX. The FIFOs have their own back-pressure mechanism with the Virtual Layer (uses the fifo\_full state). But also, the FIFOs keep track of the available credit for each VC on the ThunderX. Upon link initialization, the FIFOs module uses a "Credit Counter" to store the credits for each VC as sent from the ThunderX (in the received credit blocks in RLK). Every time the a VC FIFO forwards data words to the TLK Arbiter, it subtracts that count of forwarded data words from the Credit Counter. If the Credit Counter is zero, the FIFO abstain from forwarding data words to the Arbiter. When the ThunderX returns credit back for a particular VC, the VC FIFO captures the returned credit and add it to the Credit Counter. This is how the back-pressure is maintained by the ThunderX. 
	\item \textit{Arbitrate between different VCs to share the data words in a single Block.} A Data Block can send seven data words at once which can be shared between multiple VCs or used all with just one VC. The Arbitration mechanism defines this and it is implemented in the Arbiter module. Currently, the arbitration mechanism is very naive, it is a round robin between all the 14 VCs. A VC that has valid data words (even just one) will use the full Block and no other VC data words are inserted in the Block. However, to use the full bandwidth of the Link, a more sophisticated arbitration mechanism is needed. 
	\item \textit{Link State Machine (LSM).} The TLK implements the Link State Machine described in Section~\ref{sec:state-machine}. The LSM module makes sure the link is alive by first synchronizing the link with ThunderX, recover the link when CRC errors (or other errors) are detected, generate IDLE blocks to keep the link alive, pass credits returned to ThunderX (from VC FIFOs in the RLK), and pass VC data words forwarded from the Arbiter module through Data/Credit Blocks. 
\end{itemize}
