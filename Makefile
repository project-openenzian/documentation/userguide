###########################################################
#
# Makefile for LaTeX docs
#

DEPS=Makefile $(wildcard figures/*.pdf)

USERGUIDE=enzian_userguide
QUICKSTART=enzian_quickstart
LATEXOPTS=-interaction=nonstopmode
DVILATEXOPTS=-output-format=dvi -interaction=nonstopmode

all: userguide_pdf quickstart_pdf

userguide_pdf: $(USERGUIDE).pdf

quickstart_pdf: $(QUICKSTART).pdf

$(QUICKSTART).pdf: 
	pdflatex $(LATEXOPTS) $(QUICKSTART)
	if egrep '\\cite' $(QUICKSTART).tex && egrep 'biblatex' $(QUICKSTART).tex ; then biber $(QUICKSTART); fi
	if egrep '\\cite' $(QUICKSTART).tex && egrep 'bibtex' $(QUICKSTART).tex ; then bibtex $(QUICKSTART); fi
	if egrep '\\makeglossaries' $(QUICKSTART).tex ; then makeglossaries $(QUICKSTART); fi
	pdflatex $(LATEXOPTS) $(QUICKSTART)
	if [ -e $(QUICKSTART).toc ] ; then pdflatex $(LATEXOPTS) $(QUICKSTART) ; fi
	if [ -e $(QUICKSTART).bbl ] ; then pdflatex $(LATEXOPTS) $(QUICKSTART) ; fi
	if egrep Rerun $(QUICKSTART).log ; then pdflatex $(LATEXOPTS) $(QUICKSTART) ; fi
	if egrep Rerun $(QUICKSTART).log ; then pdflatex $(LATEXOPTS) $(QUICKSTART) ; fi
	if egrep Rerun $(QUICKSTART).log ; then pdflatex $(LATEXOPTS) $(QUICKSTART) ; fi
	# gs -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$(QUICKSTART).xpdf -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -c .setpdfwrite -f $(QUICKSTART).pdf
	$(RM) *.aux *.log *.bbl *.blg *.toc


$(USERGUIDE).pdf: $(wildcard *.tex) $(wildcard *.bib) $(DEPS)
	pdflatex $(LATEXOPTS) $(USERGUIDE)
	if egrep '\\cite' $(USERGUIDE).tex && egrep 'biblatex' $(USERGUIDE).tex ; then biber $(USERGUIDE); fi
	if egrep '\\cite' $(USERGUIDE).tex && egrep 'bibtex' $(USERGUIDE).tex ; then bibtex $(USERGUIDE); fi
	pdflatex $(LATEXOPTS) $(USERGUIDE)
	if [ -e $(USERGUIDE).toc ] ; then pdflatex $(LATEXOPTS) $(USERGUIDE) ; fi
	if [ -e $(USERGUIDE).bbl ] ; then pdflatex $(LATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(LATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(LATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(LATEXOPTS) $(USERGUIDE) ; fi
	# gs -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$(USERGUIDE).xpdf -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -c .setpdfwrite -f $(USERGUIDE).pdf
	$(RM) *.aux *.log *.bbl *.blg *.toc

$(USERGUIDE).xpdf: $(wildcard *.tex) $(wildcard *.bib) $(DEPS)
	pdflatex $(DVILATEXOPTS) $(USERGUIDE)
	bibtex $(USERGUIDE)
	pdflatex $(DVILATEXOPTS) $(USERGUIDE)
	if [ -e $(USERGUIDE).toc ] ; then pdflatex $(DVILATEXOPTS) $(USERGUIDE) ; fi
	if [ -e $(USERGUIDE).bbl ] ; then pdflatex $(DVILATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(DVILATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(DVILATEXOPTS) $(USERGUIDE) ; fi
	if egrep Rerun $(USERGUIDE).log ; then pdflatex $(DVILATEXOPTS) $(USERGUIDE) ; fi
	dvips -Ppdf -Pcmz -Pamz -t letter -D 600 -G0 -o $(USERGUIDE).ps $(USERGUIDE).dvi
	ps2pdf14 -dPDFSETTINGS=/prepress -dEmbedAllFonts=true $(USERGUIDE).ps $(USERGUIDE).pdf
	$(RM) *.aux *.log *.bbl *.blg *.toc

%.ps: %.pdf
	pdf2ps $< $@

clean:
	$(RM) *.aux *.log *.bbl *.blg *~ \#*\# *.toc *.idx
	$(RM) $(patsubst %.tex, %.ps, $(wildcard *.tex))
	$(RM) $(patsubst %.tex, %.out, $(wildcard *.tex))
	$(RM) $(patsubst %.tex, %.dvi, $(wildcard *.tex))
	$(RM) $(patsubst %.tex, %.pdf, $(wildcard *.tex))
	$(RM) enzian_quickstart.pdf

