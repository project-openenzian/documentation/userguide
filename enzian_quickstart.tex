\documentclass[a4paper,11pt]{article}

\usepackage{afterpage}
\usepackage{alltt}
\usepackage{boxedminipage}
\usepackage{caption}
\usepackage{enumitem}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{fancyvrb}
\usepackage[bottom]{footmisc}
\usepackage{geometry}
\usepackage[toc]{glossaries}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{palatino}
\usepackage{parskip}
\usepackage{subcaption}
\usepackage{tabularx}
\usepackage{url}
\usepackage{vhistory}
\usepackage{color}


\usepackage[backend=biber]{biblatex}
\addbibresource{references.bib}
\geometry{a4paper, total={170mm, 257mm}, top=20mm, left=20mm}

\input{enzian-acronyms.tex}

\fvset{frame=single,fontsize=\small}

\makeglossaries

\begin{document}
\renewcommand*\ttdefault{txtt}

\author{Enzian Project Group}
\title{\includegraphics[scale=0.37]{figures/enzian-logo}\vspace{20pt}\\
		Enzian Quickstart Guide}
	\date{Generated \today}
	
	\maketitle
%	\tableofcontents

\section{Introduction}

The goal of this document is to provide new Enzian users with an overview of the
current state of the Enzian cluster, the basics of the machines, and how to
power-on, program, and boot the machines. 
It is intended for people who are interested primarily in running software and
bitstreams on Enzian in the configuration it exists in now.
For detailed information about the various components of Enzian, please
see the User Guide \cite{repo:enzian_userguide}.
%% To learn how to program software and bitstreams for use on Enzian, please see 
%% the software development guide \cite{repo:enzian_sdg}. 


\section{Machine Overview}

Enzian is a cache-coherent 2-node asymmetric NUMA system where one node is a 48-core Marvell Thunder-X CPU and one node is a Xilinx Virtex Ultrascale+ FPGA. 
It has a maximum of 640~GiB of DDR4 RAM, and has up 480~Gb/s of network bandwidth, both split between the two nodes. 

The current series, described below, has the internal name \textbf{zuestoll}.

\subsection*{Node 0 (CPU)}
\begin{itemize}[noitemsep,nolistsep]
	\item Marvell Cavium ThunderX-1 CN8890-NT CPU @ 2~GHz (48 x ARMv8.1 cores)
	\item 128~GiB DDR4 -- 4x 32~GiB DIMMS @ 1866~MT/s
	\item PCIe Gen3 x8 slot
	\item 3 x NVMe connectors
	\item 4 x SATA connectors
	\item 2 x 40~Gb/s Ethernet QSFP28 connectors
	\item USB3, serial UARTs.
	\item JTAG
\end{itemize}

\subsection*{Node 1 (FPGA)}
\begin{itemize}[noitemsep,nolistsep]
	\item Xilinx Virtex Ultrascale+ FPGA XCVU9P-FLGB2104-E
	\item 512~GiB DDR4 (4x 128~GiB DIMMs @ 2133~MT/s) or 64 GiB DDR4 (4 x 16~GiB DIMMs @ 2400~MT/s)
	\item PCIe Gen3 x16 slot
	\item 1 x NVMe connector
	\item FMC connector
	\item 16 x 25~Gb/s serial lines in 4 x QSFP28 cages, configurable as 16 25~Gb/s or 4 x 100~Gb/s Ethernet.
	\item JTAG
\end{itemize}

\subsection*{Enzian Coherency Interconnect (ECI)}
\begin{itemize}[noitemsep,nolistsep]
	\item 24 x 10Gb/s lanes
	\item MOESI-like directory-based protocol
	\item Inter-processor interrupts
	\item Cached and uncached (I/O) accesses
	\item 128-byte cache line size
	\item Shared physical address space
\end{itemize}

\subsection*{Board Management Controller (BMC)}

\begin{itemize}[noitemsep,nolistsep]
	\item Enclustra Mercury SoM
	\item Xilinx Zynq MPSoC CPU running Linux
	\item Dedicated 1~Gb/s RJ45 Ethernet
	\item JTAG
	\item All system serial ports brought out as USB UARTs
\end{itemize}

\subsection*{Software}
\begin{itemize}[noitemsep,nolistsep]
	\item Ubuntu Linux 20.04 LTS, other OSes should boot.
	\item \gls{atf}
	\item \gls{bdk} for low-level access
\end{itemize}

\subsection*{Form factor}
\begin{itemize}[noitemsep,nolistsep]
	\item eATX-format motherboard (305~x~330~mm / 12~x~13~in)
	\item 2U Rackmount case
	\item Can accomodate double-width PCIe cards with riser
	\item 1200~W 1+1 CRPS Modular Server Power Supply
\end{itemize}



\section{Current Cluster Configuration}

\textbf{NOTE: this is a rapidly changing project and this
	configuration documentation may be out of date. We are doing our
	best to keep a stable and maintained configuration, however, as needs 
	change, this configurations may change.}

There are currently 14 functioning Enzian \textit{zuestollen} (\texttt{zuestoll01$\dots$14}).
All except \texttt{zuestoll04} are in the cluster and accessible remotely (\texttt{zuestoll14} is in our lab for things that require physical access to a machine).
You can find an overview of the machine configurations on our website\footnote{\url{https://enzian.systems/generated/cluster-info.html}}.
Both of the CPU-side 40G QSFPs are connected to a 100G switch.
The CPU0 interface comes up with access to the internet for package management,
etc, while the CPU1 interface is not assigned an IP by default.
The FPGAs have all four of their QSFPs (QSFP0-3) connected to a 100~G switch.

Each machine boots via network boot, which has to be correctly configured before
powering up an Enzian.
We provide a tool to automatically do this on \texttt{enzian-gateway}, called
\texttt{emg}.
Its documentation is accessible on \texttt{enzian-gateway} via \texttt{man emg}.
The \texttt{emg} tool contains subcommands to configure and manage disk images for
the Enzian machines.
Each user has their own pool of disk images, which is initially empty.
When acquiring an Enzian, a new boot image is created automatically containing a
known-good basic configuration of Ubuntu 20.04 LTS.
Images can be named, and only named images are persisted across boots.
To name an image, see the man page of \texttt{emg-acquire(1)}.
These network disks are small, and are \emph{not} intended to hold large amounts
of data (measurements, benchmarks, etc.).
Use the local SSD (next paragraph) for this purpose.

Each machine has a single 250~GB SATA-connected SSD for storing temporary and/or
experimental data.
There are no backups made of these disks and there is no guarantee that an overzealous
piece of software (or engineer) will not destroy all data on the drive, so it
is essential to understand that unless you hold the lock to the machine, the
data should be considered gone.

The cluster is behind \texttt{enzian-gateway.inf.ethz.ch}. 
In order to access the machines at this time, you'll need a ETH nethz account with
proper permissions for the group. 
Please get in touch with your Enzian group contact for setting up these accounts.
\texttt{enzian-gateway} also has the most recent and stable version of various
firmware (e.g. \gls{atf}, \gls{bdk}, OpenBMC \cite{openbmc_2014} available via \texttt{tftp}. 
See the user guide for details on reflashing the various components.

There is also \texttt{enzian-build.ethz.ch} which has the necessary toolchains for 
building the main Enzian FPGA project, OpenBMC, etc. as well as a significant amount
of scratch space available in \texttt{/scratch}.
A VNC session to \texttt{enzian-build} can be opened for using the Vivado/Vitis GUI.

\section{Booting Enzian}
Before you can boot an Enzian you need to reserve the machine. This is done via
the \texttt{emg} tool, specifically its \texttt{acquire} subcommand. See
\texttt{emg-acquire(1)} for a detailed description of that command. In short,
\texttt{emg acquire} creates and configures the network disk image for the
targeted Enzian. Once you have acquired a machine you can acquire the consoles
of the machine. You will be then be using an interactive Python console on the
\gls{bmc} to power on the \gls{psu}, then the CPU, and then the FPGA. If you do
not wish to program the FPGA and only use a single-socket CPU-only
configuration, you can just power on the CPU and it will boot into Linux. If you
do want to program the FPGA and bring up \gls{eci}, then you will hold the CPU
in reset at the \gls{bdk} menu after powering it on, power on the FPGA, and
program it before allowing the CPU to bring up the link.

\subsection{Consoles}

Each zuestoll has three consoles: CPU, FPGA, and \gls{bmc}.
These are made available on \texttt{enzian-gateway} using
\texttt{conserver}~\cite{man:conserver}. Note that it is currently not enforced
that you only acquire consoles of machines that you have acquired. Be sure to
respect other users and only acquire your machines' consoles unless specifically
given permission by the current owner of another machine. To see available
machines use the \texttt{emg list-machines} command:

\begin{Verbatim}[frame=single]
    noob@enzian-gateway:~$ emg list-machines
    The following machines are available
    id  name        current_user  is_up  configuration
    --  ----------  ------------  -----  -------------
    1   zuestoll01  adam          true
    2   zuestoll02                true
    3   zuestoll03  bob           true
    4   zuestoll04                false
    5   zuestoll05                true
    6   zuestoll06                true
    7   zuestoll07  eve           true
    8   zuestoll08                true
    9   zuestoll09                true
    10  zuestoll10                true
    11  zuestoll11                true
    12  zuestoll12                true
    13  zuestoll13                true
    14  zuestoll14                true
\end{Verbatim}

\noindent
In the above example, user \texttt{adam} holds the lock on \texttt{zuestoll01}.
User \texttt{bob} holds the lock on \texttt{zuestoll03}.
User \texttt{eve} holds the lock on \texttt{zuestoll07}.
The machine \texttt{zuestoll04} is \texttt{down} right now, meaning that its
console is not active.
All other \textit{zuestollen} are available (\texttt{up}.

\subsection{Powering On Enzian}

To begin the boot process, run \texttt{emg acquire zuestoll<n>} to
generate and configure the network disk for \texttt{zuestoll<n>} to boot off of.
You can use a terminal emulator such as \texttt{tmux} or \texttt{screen}, 
both of which are available on \texttt{enzian-gateway} to keep your consoles held
and to be able to switch between them as needed.

\begin{Verbatim}
	noob@enzian-gateway:~$ emg acquire zuestoll05
        Trying to boot machine zuestoll05
	No volume name provided, generating random name...
	Using snapshot source zuestoll-golden-2022-07-12
	  Logical volume "noob-generated-5da3ea6fa5" created.
	Exposing noob-generated-5da3ea6fa5 via iSCSI
	Writing tgt configuration
	Applying updates to tgt configuration
	Configuring grub
        Exposing scratch space
        noob@enzian-gateway:~$
\end{Verbatim}

Trying to acquire a machine that is already held will cause the
acquiration process to fail. Coordinate with whomever is holding the machine's lock.

\begin{Verbatim}
	noob2@enzian-gateway:~$ emg acquire zuestoll01
	Machine zuestoll01 is already reserved!
\end{Verbatim}

Once successfully acquired, you can access the \gls{bmc} console with \texttt{console zuestoll<n>-bmc}

\begin{Verbatim}
	noob@enzian-gateway:~$ console zuestoll05-bmc
	[Enter `^Ec?' for help]
\end{Verbatim}

\noindent When you press enter, you'll see one of a few things.
Here, we have the \gls{bmc} login prompt. 
The default root password is \texttt{0penBmc} (sssh! \cite{cve:default_pw})

\begin{Verbatim}
	zuestoll04-bmc login: 
\end{Verbatim}

\noindent You may also just see the command prompt. 

\begin{Verbatim}
	root@zuestoll05-bmc:~# 
\end{Verbatim}

 
\noindent
In this case, you should load the power management shell using

\begin{Verbatim}
	root@zuestoll05-bmc:~# enzian-shell bringup
\end{Verbatim}

\noindent
From here, you'll see an interactive python console indicated by \texttt{>>>}.

You should also take the associated CPU console \texttt{console zuestoll04-console}.
This is where split screen in \texttt{tmux} can be very helpful.

The power manager is written in Python and has the power to control and monitor every
regulator on the Enzian board. 
\textbf{An incorrect command can quite literally melt the very expensive components on
	the board; so it is absolutely imperative that you only use the safe functions
	outlined below. If you have ANY QUESTIONS you MUST ask somebody who knows the system}
That said, the necessary commands for booting the Enzian are quite safe and easy.

First, confirm the state of the system's power supply:

\begin{Verbatim}
    >>> print_voltage_all()
           0V9_VDD_OCT      -      -
         12V_CPU0_PSUP      -      -      -      -
         12V_CPU1_PSUP      -      -      -      -      -      -      - 
           1V5_VDD_OCT      -      -
             2V5_CPU13      -
             2V5_CPU24      -
              3V3_PSUP      -
               5V_PSUP      -      -
           BMC_VCC_3V3      -      -
            BMC_VCC_5V      -      -
          MGTAVCC_FPGA      -      -
          MGTAVTT_FPGA      -
           MGTVCCAUX_L      -
           MGTVCCAUX_R      -
               SYS_1V8      -
            SYS_2V5_13      -
            SYS_2V5_24      -
              UTIL_3V3      -      -
         VADJ_1V8_FPGA      -
           VCC1V8_FPGA      -      -
    VCCINTIO_BRAM_FPGA      -      -
           VCCINT_FPGA      -      -
              VDD_CORE      -      -
          VDD_DDRCPU13      -      -
          VDD_DDRCPU24      -      -
         VDD_DDRFPGA13      -
         VDD_DDRFPGA24      -
          VTT_DDRCPU13      -
          VTT_DDRCPU24      -
\end{Verbatim}

\noindent
The dashes (\texttt{-}) indicate a regulator is powered off. 
Now you can power on the \gls{psu} and regulators necessary for the fans, sequencers, etc.

\begin{Verbatim}
    >>> common_power_up()
        PSUP_PGOOD is True after 0.6602636068128049 (limit 2)
        Voltage 12V_CPU0_PSUP within <11.42,12.58> after 0.01157768489792943
        Voltage 12V_CPU1_PSUP within <11.42,12.58> after 0.010061481967568398
        Voltage 5V_PSUP within <4.75,5.25> after 0.008265720680356026
        Voltage 3V3_PSUP within <3.135,3.465> after 0.008221160154789686
\end{Verbatim}

\noindent
The primary \gls{psu} rails have been turned on and are in range within the allotted time.
The CPU can now be powered on.

\begin{Verbatim}
    >>> cpu_power_up()
        Voltage VDD_CORE within <0.91,0.98> after 0.008462546858936548
        Voltage 0V9_VDD_OCT within <0.87,0.93> after 0.00831432081758976
        Voltage 1V5_VDD_OCT within <1.45,1.55> after 0.01653258502483368
        Voltage 2V5_CPU13 within <2.375,2.625> after 0.016440787818282843
        Voltage 2V5_CPU24 within <2.375,2.625> after 0.01635556109249592
        Voltage VDD_DDRCPU13 within <1.14,1.26> after 0.008320264052599669
        Voltage VDD_DDRCPU24 within <1.14,1.26> after 0.008347989991307259
\end{Verbatim}

\noindent
The CPU has been successfully powered up and all voltages are within range.
In the CPU console, you will see the \gls{bdk} information appear.

\begin{Verbatim}
    Cavium SOC
    Locking L2 cache
    PASS: CRC32 verification
    Transferring to thread scheduler
    ================
    Cavium Boot Stub
    ================
    Firmware Version: 2021-11-03  10:38:19
    BDK Version: 94b9278, Branch: pipelines/135538, Built: Wed Nov 3 10:34:31 UTC 2021
    
    Board Model:    enzian_v3
    Board Revision: 1.5c
    Board Serial:   5
    
    Node:  0
    SKU:   CN8890-2000BG2601-NT-Y-G
    L2:    16384 KB
    RCLK:  2000 Mhz
    SCLK:  800 Mhz
    Boot:  SPI24(5)
    VRM:   Disabled
    Trust: Disabled, Non-secure Boot
    CCPI:  Disabled
    
    Press 'B' for boot menu (10 sec)
\end{Verbatim}

\noindent
Here you can see basic information about the system including the \gls{bdk} version (git commit), board serial number (5 = \texttt{zuestoll05}), as well as detailed information
about the CPU.
Press \texttt{b} to break the boot and hold the CPU in reset.

\begin{Verbatim}
    ============================
    Boot Options
    ============================
    N) Boot Normally
    S) Enter Setup
    D) Enter Diagnostics
    E) Enter Diagnostics, skipping Setup
    F) Select Image from Flash
    U) Change baud rate and flow control
    R) Reboot
    
    Choice:                                           
\end{Verbatim}

\noindent
At this point, if you're only using the CPU, you can press \texttt{n} and boot normally into Linux (see Section \ref{sec:boot_linux}).
If you want to program the FPGA and bring up \gls{eci}, then leave the CPU in reset and 
continue below.
Power on the FPGA

\begin{Verbatim}
    >>> fpga_power_up()
        Voltage UTIL_3V3 within <3.135,3.465> after 0.10841434309259057
        Voltage VCCINT_FPGA within <0.873,0.923> after 0.008288568817079067
        Voltage VCCINTIO_BRAM_FPGA within <0.873,0.923> after 0.10833975719287992
        Voltage VCC1V8_FPGA within <1.71,1.89> after 0.10834458796307445
        Voltage SYS_1V8 within <1.71,1.89> after 0.016447072848677635
        Voltage SYS_2V5_13 within <2.375,2.625> after 0.01644139690324664
        Voltage SYS_2V5_24 within <2.375,2.625> after 0.008215014822781086
        Voltage VDD_DDRFPGA13 within <1.14,1.26> after 0.006007685326039791
        Voltage VDD_DDRFPGA24 within <1.14,1.26> after 0.0061136470176279545
        Voltage VADJ_1V8_FPGA within <1.71,1.89> after 0.10375275695696473
        Voltage MGTAVCC_FPGA within <0.855,0.945> after 0.008343382272869349
        Voltage MGTAVTT_FPGA within <1.164,1.236> after 0.00741276191547513
        Voltage MGTVCCAUX_L within <1.71,1.89> after 0.008315557148307562
        Voltage MGTVCCAUX_R within <1.71,1.89> after 0.0083264852873981
\end{Verbatim}

\noindent
At this point, all voltage regulators will be powered on and the FPGA is ready to be programmed.

\begin{Verbatim}
    >>> print_voltage_all()
           0V9_VDD_OCT   0.91   0.90
         12V_CPU0_PSUP  11.88  12.31  12.31  12.09
         12V_CPU1_PSUP  11.87  12.19  12.22  12.16  12.09  12.11  12.09  12.09
           1V5_VDD_OCT   1.51   1.50
             2V5_CPU13   2.53
             2V5_CPU24   2.53
              3V3_PSUP   3.37
               5V_PSUP   5.11   5.12
           BMC_VCC_3V3   3.28   3.25
            BMC_VCC_5V   5.08   5.03
          MGTAVCC_FPGA   0.91   0.90
          MGTAVTT_FPGA   1.20
           MGTVCCAUX_L   1.82
           MGTVCCAUX_R   1.82
               SYS_1V8   1.82
            SYS_2V5_13   2.53
            SYS_2V5_24   2.53
              UTIL_3V3   3.31   3.30
         VADJ_1V8_FPGA   1.80
           VCC1V8_FPGA   1.80   1.80
    VCCINTIO_BRAM_FPGA   0.90   0.90
           VCCINT_FPGA   0.90   0.90
              VDD_CORE   0.96   0.96
          VDD_DDRCPU13   1.20   1.20
          VDD_DDRCPU24   1.20   1.20
         VDD_DDRFPGA13   1.20
         VDD_DDRFPGA24   1.20
          VTT_DDRCPU13   0.60
          VTT_DDRCPU24   0.60
\end{Verbatim}    

\subsubsection{Programming the FPGA} \label{sec:program_fpga}

Using Vivado is beyond the scope of this document. 
However, if you have a working bitstream, you can program it from the command line 
using a tcl script. 
At the moment, this must be done from a machine separate from \texttt{enzian-gateway} that has the appropriate Vivado toolchain installed.
\textbf{There is no access control over the JTAG chain used to program FPGAs! 
	If you pick the wrong device, you will program somebody else's FPGA. This isn't 
	likely to cause damage (except of course to your friendship with whomever's 
	experiment you just erased) but please be careful.}
Therefore, you should make sure you pick the right JTAG ID.

\begin{Verbatim}
$ /opt/Xilinx/Vitis/2022.1/bin/program_flash -jtagtargets -url \
  tcp:enzian-gateway.inf.ethz.ch:3121

****** Xilinx Program Flash
****** Program Flash v2022.1 (64-bit)
  **** SW Build 3524075 on 2022-04-13-17:42:45
    ** Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.


Connected to hw_server @ tcp:enzian-gateway.inf.ethz.ch:3121
Available targets and devices:
JTAG chain configuration
--------------------------------------------------
1 Digilent 210357A7CB93A 
    2    jsn-JTAG-SMT3-210357A7CB93A-14b31093-0  (name xcvu9p  idcode 14b31093)
3 Digilent 210357A7CB8EA 
    4    jsn-JTAG-SMT3-210357A7CB8EA-14b31093-0  (name xcvu9p  idcode 14b31093)
    5    jsn-JTAG-SMT3-210357A7CB8EA-4ba00477-0  (name arm_dap  idcode 4ba00477)
    6    jsn-JTAG-SMT3-210357A7CB8EA-0373b093-0  (name xc7z015  idcode 0373b093)
6 Digilent 210357A7CB91A 
    7    jsn-JTAG-SMT3-210357A7CB91A-14b31093-0  (name xcvu9p  idcode 14b31093)
    8    jsn-JTAG-SMT3-210357A7CB91A-4ba00477-0  (name arm_dap  idcode 4ba00477)
    9    jsn-JTAG-SMT3-210357A7CB91A-0373b093-0  (name xc7z015  idcode 0373b093)
7 Digilent 210357A7CB8FA 
5 Digilent 210357A7CB8BA 
    6    jsn-JTAG-SMT3-210357A7CB8BA-14b31093-0  (name xcvu9p  idcode 14b31093)
    7    jsn-JTAG-SMT3-210357A7CB8BA-4ba00477-0  (name arm_dap  idcode 4ba00477)
    8    jsn-JTAG-SMT3-210357A7CB8BA-0373b093-0  (name xc7z015  idcode 0373b093)
9 Digilent 210357A7CB89A 
7 Digilent 210357A7CB88A 
    8    jsn-JTAG-SMT3-210357A7CB88A-14b31093-0  (name xcvu9p  idcode 14b31093)
    9    jsn-JTAG-SMT3-210357A7CB88A-4ba00477-0  (name arm_dap  idcode 4ba00477)
    10    jsn-JTAG-SMT3-210357A7CB88A-0373b093-0  (name xc7z015  idcode 0373b093)
11 Digilent 210357A7CB8CA 
    12    jsn-JTAG-SMT3-210357A7CB8CA-14b31093-0  (name xcvu9p  idcode 14b31093)
    13    jsn-JTAG-SMT3-210357A7CB8CA-4ba00477-0  (name arm_dap  idcode 4ba00477)
    14    jsn-JTAG-SMT3-210357A7CB8CA-0373b093-0  (name xc7z015  idcode 0373b093)
12 Digilent 210357A7CB8DA 
    13    jsn-JTAG-SMT3-210357A7CB8DA-14b31093-0  (name xcvu9p  idcode 14b31093)
    14    jsn-JTAG-SMT3-210357A7CB8DA-4ba00477-0  (name arm_dap  idcode 4ba00477)
    15    jsn-JTAG-SMT3-210357A7CB8DA-0373b093-0  (name xc7z015  idcode 0373b093)
\end{Verbatim}

\noindent
The JTAG ID is the number that starts with 210 (e.g. \texttt{210357A7CB8CA}).
The correct JTAG ID for a given machine can be found on the Enzian website\footnote{\url{https://enzian.systems/generated/cluster-info.html}}.
There are three devices on the JTAG chain: the primary FPGA (\texttt{xcvu9p}), the
\gls{bmc} CPU (\texttt{arm\_dap}), and the \gls{bmc} FPGA (\texttt{xc7z015}).

\noindent
A sample tcl script that can be used to program the FPGA on \texttt{zuestoll05} with the JTAG ID \\ \texttt{210357A7CB8CA} on the primary hardware server \texttt{enzian-gateway.ethz.ch:3121} with bitstream files \\
\texttt{cool\_experiment.\{bit,ltx\}} is shown below:

\begin{Verbatim}
    open_hw_manager
    connect_hw_server -url enzian-gateway.ethz.ch:3121
    current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357A7CB8CA]
    open_hw_target
    set_property PROGRAM.FILE {cool_experiment.bit} [get_hw_devices xcvu9p_0]
    set_property PROBES.FILE {cool_experiment.ltx} [get_hw_devices xcvu9p_0]
    set_property FULL_PROBES.FILE {cool_experiment.ltx} [get_hw_devices xcvu9p_0]
    current_hw_device [lindex [get_hw_devices] 0]
    program_hw_devices [lindex [get_hw_devices] 0]
    refresh_hw_device [lindex [get_hw_devices] 0]
    quit
\end{Verbatim}

\noindent
This experiment \texttt{cool\_experiment.tcl} can be run on \texttt{enzian-build} with the command 

\begin{Verbatim}
    /opt/Xilinx/Vivado/2022.1/bin/vivado -mode batch -source cool_experiment.tcl
\end{Verbatim}

It can be done via \texttt{enzian-build} which also has a number of test bitstreams.

\begin{Verbatim}
noob@enzian-build:~/ETH/enzian$ /opt/Xilinx/Vivado/2022.1/bin/vivado -mode batch 
	-source program_bitstream.tcl 

****** Vivado v2022.1 (64-bit)
  **** SW Build 3526262 on Mon Apr 18 15:47:01 MDT 2022
  **** IP Build 3524634 on Mon Apr 18 20:55:01 MDT 2022
    ** Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.

source program_bitstream.tcl
# open_hw_manager
# connect_hw_server -url enzian-gateway.ethz.ch:3121
INFO: [Labtools 27-2285] Connecting to hw_server url TCP:enzian-gateway:3121
INFO: [Labtools 27-3415] Connecting to cs_server url TCP:localhost:3042
INFO: [Labtools 27-3414] Connected to existing cs_server.
# current_hw_target [get_hw_targets */xilinx_tcf/Digilent/210357A7CB8CA]
# open_hw_target
INFO: [Labtoolstcl 44-466] Opening hw_target 
    enzian-gateway.ethz.ch:3121/xilinx_tcf/Digilent/210357A7CB8CA
# set_property PROGRAM.FILE {/storage/bitstreams/eci_loopback_test.bit} 
    [get_hw_devices xcvu9p_0]
# set_property PROBES.FILE {/storage/bitstreams/eci_loopback_test.ltx} 
    [get_hw_devices xcvu9p_0]
# set_property FULL_PROBES.FILE {/storage/bitstreams/eci_loopback_test.ltx}
    [get_hw_devices xcvu9p_0]
# current_hw_device [lindex [get_hw_devices] 0]
# program_hw_devices [lindex [get_hw_devices] 0]
INFO: [Labtools 27-3164] End of startup status: HIGH
program_hw_devices: Time (s): cpu = 00:01:20 ; elapsed = 00:01:20 . Memory (MB): 
    peak = 2162.742 ; gain = 0.000 ; free physical = 16196 ; free virtual = 31983
# refresh_hw_device [lindex [get_hw_devices] 0]
INFO: [Labtools 27-2302] Device xcvu9p (JTAG device index = 0) is programmed with 
    a design that has 2 ILA core(s).
INFO: [Labtools 27-2302] Device xcvu9p (JTAG device index = 0) is programmed with 
    a design that has 1 VIO core(s).
INFO: [Labtools 27-1889] Uploading output probe values for VIO core [hw_vio_1]
# quit
INFO: [Common 17-206] Exiting Vivado at Mon Nov 22 10:19:24 2022...

\end{Verbatim}

Once this is successfully programmed, you can return to the CPU console and begin
the boot process by pressing \texttt{n} at the \gls{bdk} boot menu.

\begin{Verbatim}[commandchars=\\\{\}]
Choice: N

Loading image file '/fatfs/init.bin'
---
Cavium SOC
Locking L2 cache
PASS: CRC32 verification
Transferring to thread scheduler
Using configuration from previous image
===========
Cavium Init
===========
BDK Enzian Version: 94b9278, Branch: pipelines/135538, Built: Wed Nov 3 10:34:31 UTC 2021

ERROR: N0.TWSI0: Completion poll timed out

ERROR: N0.TWSI0: Completion poll timed out

ERROR: N0.TWSI0: Completion poll timed out

ERROR: N0.TWSI0: Completion poll timed out

N0.LMC0.DIMM0: 32768 MB, DDR4 RDIMM 2Rx4 ECC, p/n: 9965694-026.B00G, s/n: 611384230, 1.2V
N0.LMC0 Configuration Completed: 32768 MB
N0.LMC1.DIMM0: 32768 MB, DDR4 RDIMM 2Rx4 ECC, p/n: 9965694-026.B00G, s/n: 611378817, 1.2V
N0.LMC1 Configuration Completed: 32768 MB
N0.LMC2.DIMM0: 32768 MB, DDR4 RDIMM 2Rx4 ECC, p/n: 9965694-026.B00G, s/n: 611378956, 1.2V
N0.LMC2 Configuration Completed: 32768 MB
N0.LMC3.DIMM0: 32768 MB, DDR4 RDIMM 2Rx4 ECC, p/n: 9965694-026.B00G, s/n: 1592849256, 1.2V
N0.LMC3 Configuration Completed: 32768 MB
Node 0: DRAM: 131072 MB, 940 MHz, DDR4 RDIMM
Enzian init. TODO.
\textcolor{red}{Starting ECI links}
\textcolor{red}{CDR lock QLM8:1 QLM9:1 QLM10:1 QLM11:1 QLM12:1 QLM13:1}
\textcolor{red}{N0.CCPI Lanes([] is good):[0][1][2][3][4][5][6][7][8][9][10][11][12][13][14][15][16][17][18}
\textcolor{red}{][19][20][21][22][23]}
Initialize BGX
Initialize USB
Initialize PCIe
N0.PCIe0: Link timeout, probably the slot is empty
N0.PCIe2: Link active, 1 lanes, speed gen1
N0.PCIe4: Link timeout, probably the slot is empty
N0.PCIe5: Link timeout, probably the slot is empty
Initialize TWSI
Initialize MDIO
Initialize VSC8248 (40GbE) PHYs
N0.BGX0: Detected VSC8248 revision 3 PHY at TWSI 0x40
N0.BGX1: Detected VSC8248 revision 3 PHY at TWSI 0x41
Processing DeviceTrees...
Loading image at /boot:0x400000
---
WARNING: No BMC-BOOT-TWSI-BUS is found
WARNING: No BMC-BOOT-TWSI-ADDR is found
WARNING: No BMC-IPMI-TWSI-BUS is found
WARNING: No BMC-IPMI-TWSI-ADDR is found
WARNING: No GPIO-SHUTDOWN-CTL-IN is found
WARNING: No GPIO-SHUTDOWN-CTL-OUT is found
======================
BOARD MODEL = enzian_v3
MULTI MODE = 0
BMC BOOT TWSI bus=0xffffffff, addr=0xffffffff
BMC IPMI TWSI bus=0xffffffff, addr=0xffffffff
GPIO Shutdown pin IN = 0xffffffff
GPIO Shutdown pin OUT = 0xffffffff
======================
Mark memory region 0:: 0 to 300000 as secure (2)
Mark memory region 1:: 400000 to 2000000000 as non-secure (1)
#
Using TWSI func = 0x48
Using TWSI func = 0x49
Using TWSI func = 0x4a
Using TWSI func = 0x4b
Using TWSI func = 0x4c
Using TWSI func = 0x4d
##############
#
#########################################
Booting trusted firmware boot loader stage 1
v0.3(release):ThunderX-Firmware-Release-1.22.12-build_07
Built : 08:56:00, Apr 15 2021
Environment: FDT @: 0x20000

#
Loading Bootloader..
############################################
...
######
WARNING: No BMC-BOOT-TWSI-BUS is found
WARNING: No BMC-BOOT-TWSI-ADDR is found
WARNING: No BMC-IPMI-TWSI-BUS is found
WARNING: No BMC-IPMI-TWSI-ADDR is found
WARNING: No GPIO-SHUTDOWN-CTL-IN is found
WARNING: No GPIO-SHUTDOWN-CTL-OUT is found
PROGRESS CODE: V03020003 I0
PROGRESS CODE: V03020002 I0
PROGRESS CODE: V03020003 I0
PROGRESS CODE: V03021001 I0
\end{Verbatim}

From here, the EDK2 will choose which UEFI method to boot from, the default
being a network boot via the first CPU interface (CPU0).

\subsubsection{Booting Linux} \label{sec:boot_linux}

Currently, Enzian is running a fairly stock Ubuntu 20.04 image.
Its disk is provided via the network over the iSCSI protocol by
\texttt{enzian-gateway}.
Packages can be installed in the normal way.
By default, the network disk is ephermal and will be deleted once the user
disconnects from the BMC console.
Network disks can be persisted by naming as part of the \texttt{emg boot}
invocation, and available network disks to boot from can be seen by running
\texttt{emg list-images}.
The standard Linux build tools should be available but you are free
to install packages you need.
To make use of various aspects of Enzian, you may need kernel modules
to map FPGA memory into the address space in the correct way. 
Until there is a default FPGA shell, these modules will be distributed with
the associated bitstreams.

The default username/password is \texttt{enzian}/\texttt{enzian}.
This user has \texttt{sudo} rights and thus the ability to install necessary 
packages for development.
The network disk that an Enzian boots from belongs only to your user, so feel
free to put things wherever you like.
Currently there is a single 250~GB SATA SSD for storing temporary data, such as
benchmark data, measurements and so on.
More critically, there are no backups, no serious security measures, and no
promises that a system won't be broken, reimaged, or corrupted at any given time.
Please \textit{please} \textbf{please} regularly copy necessary data off, especially
when giving up the console lock.

\section{Shutting it Down}

In order to safely powerdown the system \texttt{sudo halt} will shutdown Linux.
Then from the BMC console, \texttt{power\_down()} will turn off all regulators.
The machine is powered off.

You can disconnect from the consoles using \texttt{\string^Ec.} (that is, \texttt{CTRL+e}, 
then \texttt{c}, then \texttt{.}).
The \texttt{emg} tool will clean up after itself once you release the
machine via \texttt{emg release <machine>}, deleting ephemeral network
images and resetting the network boot configuration. Then the machine is
returned back into the free pool.

\begin{Verbatim}
	>>> power_down()
	>>> [disconnect]
	noob@enzian-gateway:~$
	noob@enzian-gateway:~$ emg release zustoll05
	Remove grub config for zuestoll05
	Unexpose volume via TGT
	Removing target file /etc/tgt/conf.d/noob-generated-5da3ea6fa5.conf
	Grabbing target id from tgtadm... 1
	Deleting target 1
	Removing generated volume noob-generated-5da3ea6fa5
	Destroying unpersisted volume enzian-images/noob-generated-5da3ea6fa5
	  Logical volume "noob-generated-5da3ea6fa5" successfully removed
	noob@enzian-gateway:~$
\end{Verbatim}

A named volume is \emph{not} deleted automatically.
Please be sure to clean up after yourself once you don't need it anymore.

\begin{Verbatim}
	>>> [disconnect]
	noob@enzian-gateway:~$ emg release zuestoll07
	Remove grub config for zuestoll07
	Unexpose volume via TGT
	Removing target file /etc/tgt/conf.d/noob-persisted-image.conf
	Grabbing target id from tgtadm... 1
	Deleting target 1
	Volume name was passed, not deleting your volume.
	Be sure to clean it up later!
	noob@enzian-gateway:~$
\end{Verbatim}

\printglossaries

%\bibliography{references}
%\bibliographystyle{IEEEtran}

\printbibliography


\end{document}
